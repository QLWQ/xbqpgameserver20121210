call PROC_SYN_VIEW("aitreasurechange");
call PROC_SYN_VIEW("matchplayerrank");
call PROC_SYN_VIEW("platformgamecount");
call PROC_SYN_VIEW("platformonlinecount");
call PROC_SYN_VIEW("playerdeposit");
call PROC_SYN_VIEW("playerenrollment");
call PROC_SYN_VIEW("playersignin");
call PROC_SYN_VIEW("rechargeplatformorder");
call PROC_SYN_VIEW("recordaccountreg");
call PROC_SYN_VIEW("recordailogingame");
call PROC_SYN_VIEW("recordailogoutgame");
call PROC_SYN_VIEW("recordbankchange");
call PROC_SYN_VIEW("recordboard101");
call PROC_SYN_VIEW("recordboard105");
call PROC_SYN_VIEW("recordboard106");
call PROC_SYN_VIEW("recordboard141");
call PROC_SYN_VIEW("recordboard151");
call PROC_SYN_VIEW("recordboard161");
call PROC_SYN_VIEW("recordboard171");
call PROC_SYN_VIEW("recordboard181");
call PROC_SYN_VIEW("recordboard191");
call PROC_SYN_VIEW("recordboard200");
call PROC_SYN_VIEW("recordboard201");
call PROC_SYN_VIEW("recordboard202");
call PROC_SYN_VIEW("recordboard203");
call PROC_SYN_VIEW("recordboard204");
call PROC_SYN_VIEW("recordboard205");
call PROC_SYN_VIEW("recordchangepassword");
call PROC_SYN_VIEW("recordcloseaccount");
call PROC_SYN_VIEW("recorddamachange");
call PROC_SYN_VIEW("recordextinfo101");
call PROC_SYN_VIEW("recordextinfo171");
call PROC_SYN_VIEW("recordgamerebatebalance");
call PROC_SYN_VIEW("recordgamerebatedetail");
-- call PROC_SYN_VIEW("recordgamerebate");
call PROC_SYN_VIEW("recordplayerdrop");
call PROC_SYN_VIEW("recordplayerreconnect");
call PROC_SYN_VIEW("recorduserbecome");
call PROC_SYN_VIEW("recorduserbindphone");
call PROC_SYN_VIEW("recorduserdailyrank");
call PROC_SYN_VIEW("recorduserlogingame");
call PROC_SYN_VIEW("recorduserlogoutgame");
call PROC_SYN_VIEW("recordusermailreceive");
call PROC_SYN_VIEW("recordusermailsend");
call PROC_SYN_VIEW("recordusermonthlyrank");
call PROC_SYN_VIEW("recordusernicknameedit");
call PROC_SYN_VIEW("recorduserplatformlogin");
call PROC_SYN_VIEW("recorduserplatformlogout");
call PROC_SYN_VIEW("recorduserscorepergame");
call PROC_SYN_VIEW("redblackrewardpool");
call PROC_SYN_VIEW("userbetscorechange");
call PROC_SYN_VIEW("usertreasurechange");



/* **************************************************
其他视图构建
* *************************************************/
CREATE SQL SECURITY DEFINER VIEW `v_statis_regcount` AS 
select count(0) AS `regCount`,date_format(from_unixtime(`v_recordaccountreg`.`RecordTime`),'%Y-%m-%d') AS `RecordTime`,`v_recordaccountreg`.`ChannelNO` AS `ChannelNO` from `v_recordaccountreg` group by date_format(from_unixtime(`v_recordaccountreg`.`RecordTime`),'%Y-%m-%d'),`v_recordaccountreg`.`ChannelNO`;

CREATE SQL SECURITY DEFINER VIEW `v_statis_logincount` AS 
select count(distinct `v_recorduserplatformlogin`.`UserID`) AS `loginCount`,`v_recorduserplatformlogin`.`ChannelNO` AS `ChannelNO`,date_format(from_unixtime(`v_recorduserplatformlogin`.`LoginTime`),'%Y-%m-%d') AS `LoginTime` from `v_recorduserplatformlogin` group by `v_recorduserplatformlogin`.`ChannelNO`,date_format(from_unixtime(`v_recorduserplatformlogin`.`LoginTime`),'%Y-%m-%d');

CREATE SQL SECURITY DEFINER VIEW `v_statis_logingamecount` AS 
select date_format(from_unixtime(`a`.`RecordTime`),'%Y-%m-%d') AS `RecordTime`,`a`.`ChannelNO` AS `ChannelNO`,count(distinct `a`.`UserID`) AS `LoginGameCount` from `v_recorduserlogingame` `a` group by date_format(from_unixtime(`a`.`RecordTime`),'%Y-%m-%d'),`a`.`ChannelNO`;

CREATE SQL SECURITY DEFINER VIEW `v_statis_firstrecharge` AS 
select `v_rechargeplatformorder`.`UserID` AS `UserID`,`v_rechargeplatformorder`.`ChannelNO` AS `ChannelNO`,`v_rechargeplatformorder`.`PayAmount` AS `PayAmount`,`v_rechargeplatformorder`.`RecordTime` AS `RecordTime` from `v_rechargeplatformorder` group by `v_rechargeplatformorder`.`UserID`,`v_rechargeplatformorder`.`ChannelNO` order by `v_rechargeplatformorder`.`RecordTime`;

CREATE SQL SECURITY DEFINER VIEW `v_statis_firstrechargecount` AS 
select `cc`.`RecordTime` AS `RecordTime`,`cc`.`ChannelNO` AS `ChannelNO`,sum(`cc`.`TotalRechargeMoney`) AS `totalFirstRecharge`,count(distinct `cc`.`UserID`) AS `firstRechargeCount` from (select `aa`.`RecordTime` AS `RecordTime`,`aa`.`ChannelNO` AS `ChannelNO`,`aa`.`TotalRechargeMoney` AS `TotalRechargeMoney`,`aa`.`UserID` AS `UserID` from (((select date_format(from_unixtime(`a`.`RecordTime`),'%Y-%m-%d') AS `RecordTime`,`a`.`ChannelNO` AS `ChannelNO`,sum(`a`.`PayAmount`) AS `TotalRechargeMoney`,`a`.`UserID` AS `UserID` from `recordlog`.`v_rechargeplatformorder` `a` where (`a`.`PayAmount` > 0) group by date_format(from_unixtime(`a`.`RecordTime`),'%Y-%m-%d'),`a`.`ChannelNO`,`a`.`UserID`)) `aa` join (select `v_recordaccountreg`.`ChannelNO` AS `ChannelNO`,date_format(from_unixtime(`v_recordaccountreg`.`RecordTime`),'%Y-%m-%d') AS `RecordTime`,`v_recordaccountreg`.`UserID` AS `UserID` from `recordlog`.`v_recordaccountreg`) `bb`) where ((`aa`.`UserID` = `bb`.`UserID`) and (`aa`.`RecordTime` = `bb`.`RecordTime`))) `cc` group by `cc`.`RecordTime`,`cc`.`ChannelNO`;

CREATE SQL SECURITY DEFINER VIEW `v_statis_gaincount` AS 
select count(0) AS `GainCount`,sum(`v_usertreasurechange`.`GainCount`) AS `GainMoney`,`v_usertreasurechange`.`ChannelNO` AS `ChannelNO`,date_format(from_unixtime(`v_usertreasurechange`.`RecordTime`),'%Y-%m-%d') AS `RecordTime` from `v_usertreasurechange` group by `v_usertreasurechange`.`ChannelNO`,date_format(from_unixtime(`v_usertreasurechange`.`RecordTime`),'%Y-%m-%d');

CREATE SQL SECURITY DEFINER VIEW `v_statis_gametimelong` AS 
select `v_recorduserlogoutgame`.`ChannelNO` AS `ChannelNO`,sum(`v_recorduserlogoutgame`.`GameTimeCount`) AS `GameTimeCount`,date_format(from_unixtime(`v_recorduserlogoutgame`.`RecordTime`),'%Y-%m-%d') AS `RecordTime` from `v_recorduserlogoutgame` group by `v_recorduserlogoutgame`.`ChannelNO`,date_format(from_unixtime(`v_recorduserlogoutgame`.`RecordTime`),'%Y-%m-%d');

CREATE SQL SECURITY DEFINER VIEW `v_statis_halfhourusercount` AS 
select date_format(from_unixtime(`v_recorduserlogoutgame`.`RecordTime`),'%Y-%m-%d') AS `RecordTime`,count(0) AS `HalfHourUserCount`,`v_recorduserlogoutgame`.`ChannelNO` AS `ChannelNO`,sum(`v_recorduserlogoutgame`.`GameTimeCount`) AS `TimeLength` from `v_recorduserlogoutgame` group by `v_recorduserlogoutgame`.`ChannelNO`,date_format(from_unixtime(`v_recorduserlogoutgame`.`RecordTime`),'%Y-%m-%d') having (`TimeLength` >= 1800);

CREATE SQL SECURITY DEFINER VIEW `v_statis_loginusercount` AS 
select count(0) AS `LoginUserCount`,`a`.`ChannelNO` AS `ChannelNO`,date_format(from_unixtime(`a`.`LoginTime`),'%Y-%m-%d') AS `LoginTime` from `v_recorduserplatformlogin` `a` group by `a`.`ChannelNO`,date_format(from_unixtime(`a`.`LoginTime`),'%Y-%m-%d');

CREATE SQL SECURITY DEFINER VIEW `v_statis_userlogingamecount` AS 
select date_format(from_unixtime(`a`.`RecordTime`),'%Y-%m-%d') AS `RecordTime`,`a`.`ChannelNO` AS `ChannelNO`,`a`.`UserID` AS `UserID`,count(0) AS `userLoginGameCount` from `v_recorduserlogingame` `a` group by date_format(from_unixtime(`a`.`RecordTime`),'%Y-%m-%d'),`a`.`ChannelNO`,`a`.`UserID`;

CREATE SQL SECURITY DEFINER VIEW `v_newgameuser` AS 
select `aa`.`ChannelNO` AS `ChannelNO`,`aa`.`RecordTime` AS `RecordTime`,`aa`.`UserID` AS `UserID`,`aa`.`userLoginGameCount` AS `newGameCount` from (`recordlog`.`v_statis_userlogingamecount` `aa` join (select `v_recordaccountreg`.`ChannelNO` AS `ChannelNO`,date_format(from_unixtime(`v_recordaccountreg`.`RecordTime`),'%Y-%m-%d') AS `RecordTime`,`v_recordaccountreg`.`UserID` AS `UserID` from `recordlog`.`v_recordaccountreg`) `bb`) where ((`aa`.`UserID` = `bb`.`UserID`) and (`aa`.`RecordTime` = `bb`.`RecordTime`));

CREATE SQL SECURITY DEFINER VIEW `v_statis_newgamecount` AS 
select `v_newgameuser`.`ChannelNO` AS `ChannelNO`,`v_newgameuser`.`RecordTime` AS `RecordTime`,count(distinct `v_newgameuser`.`UserID`) AS `newGameCount` from `recordlog`.`v_newgameuser` group by `v_newgameuser`.`ChannelNO`,`v_newgameuser`.`RecordTime`;

CREATE SQL SECURITY DEFINER VIEW `v_statis_ordercount` AS 
select count(0) AS `OrderCount`,`rechargeplatformorder`.`ChannelNO` AS `ChannelNO`,date_format(from_unixtime(`rechargeplatformorder`.`RecordTime`),'%Y-%m-%d') AS `RecordTime` from `rechargeplatformorder` group by `rechargeplatformorder`.`ChannelNO`,date_format(from_unixtime(`rechargeplatformorder`.`RecordTime`),'%Y-%m-%d');

CREATE SQL SECURITY DEFINER VIEW `v_statis_platformwealth` AS 
select `v_usertreasurechange`.`ChannelNO` AS `ChannelNO`,date_format(from_unixtime(`v_usertreasurechange`.`RecordTime`),'%Y-%m-%d') AS `RecordTime`,sum(`v_usertreasurechange`.`GainCount`) AS `GainCount` from `v_usertreasurechange` group by `v_usertreasurechange`.`ChannelNO`,date_format(from_unixtime(`v_usertreasurechange`.`RecordTime`),'%Y-%m-%d');

CREATE SQL SECURITY DEFINER VIEW `v_statis_rechargecount` AS 
select `a`.`channel_id` AS `ChannelNO`,count(distinct `a`.`user_id`) AS `RechargeCount`,date_format(`a`.`create_time`,'%Y-%m-%d') AS `RecordTime`,sum(`a`.`pay_fee`) AS `TotalRechargeMoney` from (select `platform`.`sys_pay_order`.`channel_id` AS `channel_id`,`platform`.`sys_pay_order`.`user_id` AS `user_id`,`platform`.`sys_pay_order`.`create_time` AS `create_time`,`platform`.`sys_pay_order`.`pay_fee` AS `pay_fee` from `platform`.`sys_pay_order` where ((`platform`.`sys_pay_order`.`status` = 1) and (`platform`.`sys_pay_order`.`handler_type` = 1)) union all select `platform`.`sys_pay_order`.`channel_id` AS `channel_id`,`platform`.`sys_pay_order`.`user_id` AS `user_id`,`platform`.`sys_pay_order`.`create_time` AS `create_time`,`platform`.`sys_pay_order`.`pay_fee` AS `pay_fee` from `platform`.`sys_pay_order` where ((`platform`.`sys_pay_order`.`status` = 1) and (`platform`.`sys_pay_order`.`handler_type` = 2)) union all select `platform`.`sys_pay_order`.`channel_id` AS `channel_id`,`platform`.`sys_pay_order`.`user_id` AS `user_id`,`platform`.`sys_pay_order`.`create_time` AS `create_time`,`platform`.`sys_pay_order`.`pay_fee` AS `pay_fee` from `platform`.`sys_pay_order` where ((`platform`.`sys_pay_order`.`status` = 1) and (`platform`.`sys_pay_order`.`handler_type` = 3))) `a` group by `a`.`channel_id`,date_format(`a`.`create_time`,'%Y-%m-%d');

CREATE SQL SECURITY DEFINER VIEW `v_day_report` AS
select `c`.`regCount` AS `regCount`,`b`.`newGameCount` AS `newGameCount`,`a`.`loginCount` AS `loginCount`,`d`.`HalfHourUserCount` AS `halfHourUserCount`,(`d`.`TimeLength` / 60) AS `timeLength`,(`e`.`GameTimeCount` / 60) AS `gameTimeCount`,((`e`.`GameTimeCount` / 60) / `l`.`LoginGameCount`) AS `newUserAvgGameTime`,`f`.`LoginUserCount` AS `loginUserCount`,`a`.`loginCount` AS `dailyActiveCount`,`g`.`OrderCount` AS `orderCount`,`h`.`RechargeCount` AS `rechargeCount`,`h`.`TotalRechargeMoney` AS `totalRechargeMoney`,(`h`.`TotalRechargeMoney` / `h`.`RechargeCount`) AS `arpu`,(`h`.`RechargeCount` / `a`.`loginCount`) AS `rechargeRate`,`k`.`GainCount` AS `gainCount`,`l`.`LoginGameCount` AS `LoginGameCount`,`a`.`LoginTime` AS `recordTime`,`a`.`ChannelNO` AS `channelId` from ((((((((((`recordlog`.`v_statis_logincount` `a` left join `recordlog`.`v_statis_newgamecount` `b` on(((`a`.`ChannelNO` = `b`.`ChannelNO`) and (`a`.`LoginTime` = `b`.`RecordTime`)))) left join `recordlog`.`v_statis_regcount` `c` on(((`a`.`ChannelNO` = `c`.`ChannelNO`) and (`a`.`LoginTime` = `c`.`RecordTime`)))) left join `recordlog`.`v_statis_halfhourusercount` `d` on(((`a`.`ChannelNO` = `d`.`ChannelNO`) and (`a`.`LoginTime` = `d`.`RecordTime`)))) left join `recordlog`.`v_statis_gametimelong` `e` on(((`a`.`ChannelNO` = `e`.`ChannelNO`) and (`a`.`LoginTime` = `e`.`RecordTime`)))) left join `recordlog`.`v_statis_loginusercount` `f` on(((`a`.`ChannelNO` = `f`.`ChannelNO`) and (`a`.`LoginTime` = `f`.`LoginTime`)))) left join `recordlog`.`v_statis_ordercount` `g` on(((`a`.`ChannelNO` = `g`.`ChannelNO`) and (`a`.`LoginTime` = `g`.`RecordTime`)))) left join `recordlog`.`v_statis_rechargecount` `h` on(((`a`.`ChannelNO` = `h`.`ChannelNO`) and (`a`.`LoginTime` = `h`.`RecordTime`)))) left join `recordlog`.`v_statis_gaincount` `i` on(((`a`.`ChannelNO` = `i`.`ChannelNO`) and (`a`.`LoginTime` = `i`.`RecordTime`)))) left join `recordlog`.`v_statis_platformwealth` `k` on(((`a`.`ChannelNO` = `k`.`ChannelNO`) and (`a`.`LoginTime` = `k`.`RecordTime`)))) left join `recordlog`.`v_statis_logingamecount` `l` on(((`a`.`ChannelNO` = `l`.`ChannelNO`) and (`a`.`LoginTime` = `l`.`RecordTime`)))) order by `a`.`LoginTime` desc;

CREATE SQL SECURITY DEFINER VIEW `v_echar_report` AS
select `a`.`LoginTime` AS `recordTime`,`a`.`ChannelNO` AS `channelId`,`a`.`loginCount` AS `loginCount`,`c`.`regCount` AS `regCount`,`d`.`RechargeCount` AS `rechargeCount`,`d`.`TotalRechargeMoney` AS `totalRechargeMoney`,`f`.`LoginGameCount` AS `loginGameCount`,`g`.`LoginUserCount` AS `loginUserCount` from ((((`recordlog`.`v_statis_logincount` `a` left join `recordlog`.`v_statis_regcount` `c` on(((`a`.`ChannelNO` = `c`.`ChannelNO`) and (`a`.`LoginTime` = `c`.`RecordTime`)))) left join `recordlog`.`v_statis_rechargecount` `d` on(((`a`.`ChannelNO` = `d`.`ChannelNO`) and (`a`.`LoginTime` = `d`.`RecordTime`)))) left join `recordlog`.`v_statis_logingamecount` `f` on(((`a`.`ChannelNO` = `f`.`ChannelNO`) and (`a`.`LoginTime` = `f`.`RecordTime`)))) left join `recordlog`.`v_statis_loginusercount` `g` on(((`a`.`ChannelNO` = `g`.`ChannelNO`) and (`a`.`LoginTime` = `g`.`LoginTime`))));
