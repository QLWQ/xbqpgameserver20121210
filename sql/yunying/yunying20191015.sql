
-- ----------------------------
-- Table structure for RecordBoard202
-- ----------------------------
DROP TABLE IF EXISTS `RecordBoard205`;
CREATE TABLE `RecordBoard205` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` varchar(50) DEFAULT NULL COMMENT '与上文表：RecordUserScorePerGame 的牌局ID相同。生成规则:时间戳（到秒）+ 最小游戏类型 + 用户ID（仅取地主的用户ID，或者牌局内随机一个玩家的ID）',
  `UserID` int(11) DEFAULT NULL COMMENT '用户ID',
  `PlayerCnt` int(11) DEFAULT NULL COMMENT '本桌玩家人数',
  `BankerDim` int(11) DEFAULT NULL COMMENT '庄家牌型',
  `BankerPoint` int(11) DEFAULT NULL COMMENT '庄家点数',
  `BankCardRecord` varchar(128) DEFAULT NULL COMMENT '庄家具体牌面',
  `CardRecord` varchar(1000) DEFAULT NULL COMMENT '自己牌型, json数据',
  `NickName` varchar(1000) DEFAULT NULL COMMENT '昵称',
  `BeforeCoin` int(11) DEFAULT NULL COMMENT '玩家之前金币',
  `ChangeCoin` int(11) DEFAULT NULL COMMENT '变化金币',
  `AfterCoin` int(11) DEFAULT NULL COMMENT '玩家之后金币',
  `Tax` int(11) DEFAULT NULL COMMENT '税收',
  `TotalBet` int(11) DEFAULT NULL COMMENT '下注总额',
  `StartTime` int(11) DEFAULT NULL COMMENT '该局开始时间',
  `EndTime` int(11) DEFAULT NULL COMMENT '该局结束时间',
  `GameTypeID` int(11) DEFAULT NULL COMMENT '最小游戏类型ID',
  `ChannelNO` varchar(50) DEFAULT NULL COMMENT '渠道号',
  PRIMARY KEY (`ID`),
  KEY `StartTime_index` (`StartTime`) USING BTREE,
  KEY `RecordID_index` (`RecordID`) USING BTREE,
  KEY `UserID` (`UserID`),
  KEY `EndTime` (`EndTime`),
  KEY `StartTime_EndTime` (`StartTime`,`EndTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='21点牌局记录';
