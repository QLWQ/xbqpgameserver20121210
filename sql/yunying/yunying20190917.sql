-- --------------------------
-- Table structure for PlayerEnrollment
-- --------------------------
DROP TABLE IF EXISTS `PlayerEnrollment`;
CREATE TABLE `PlayerEnrollment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) DEFAULT NULL COMMENT '用户ID',
  `Fee` int(11) DEFAULT NULL COMMENT '报名费',
  `OperateType` int(11) DEFAULT NULL COMMENT '0 主界面报名成功；1:主界面取消报名；2  报名失败',
  `RecordTime` int(11) DEFAULT NULL COMMENT '操作时间',
  `GameTypeID` int(11) DEFAULT NULL COMMENT '最小游戏类型',
  `MatchGuid` varchar(50) DEFAULT NULL COMMENT '登录关联标识',
  `BusinessGuid` varchar(50) DEFAULT NULL COMMENT '业务关联标识',
  `ChannelNO` varchar(50) DEFAULT NULL COMMENT '渠道号',
  PRIMARY KEY (`ID`),
  KEY `RecordTime` (`RecordTime`),
  KEY `BusinessGuid` (`BusinessGuid`),
  KEY `UserID` (`UserID`),
  KEY `ChannelNO` (`ChannelNO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------
-- Table structure for MatchPlayerRank
-- --------------------------
DROP TABLE IF EXISTS `MatchPlayerRank`;
CREATE TABLE `MatchPlayerRank` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) DEFAULT NULL,
  `Matches` varchar(50) DEFAULT NULL COMMENT '报名场次',
  `MatchRank` int(11) DEFAULT NULL COMMENT '比赛排名',
  `MatchPoint` int(11) DEFAULT NULL COMMENT '比赛积分',
  `BoardAmount` int(11) DEFAULT NULL COMMENT '该场比赛累计局',
  `StartTime` int(11) DEFAULT NULL COMMENT '比赛开始时间',
  `EndTime` int(11) DEFAULT NULL COMMENT '比赛结束时间',
  `GameTypeID` int(11) DEFAULT NULL COMMENT '最小游戏类型',
  `MatchGuid` varchar(50) DEFAULT NULL COMMENT '登录关联标识',
  `BusinessGuid` varchar(50) DEFAULT NULL COMMENT '业务关联标识',
  `ChannelNO` varchar(50) DEFAULT NULL COMMENT '渠道号',
  PRIMARY KEY (`ID`),
  KEY `Idx_MatchPlayerRank_StartTime` (`StartTime`),
  KEY `Idx_MatchPlayerRank_EndTime` (`EndTime`),
  KEY `BusinessGuid` (`BusinessGuid`),
  KEY `UserID` (`UserID`),
  KEY `ChannelNO` (`ChannelNO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='比赛类玩家排名表';

-- ----------------------------
-- Table structure for RecordBoard202
-- ----------------------------
DROP TABLE IF EXISTS `RecordBoard202`;
CREATE TABLE `RecordBoard202` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` varchar(50) DEFAULT NULL COMMENT '与上文表：RecordUserScorePerGame 的牌局ID相同。生成规则:时间戳（到秒）+ 最小游戏类型 + 用户ID（仅取地主的用户ID，或者牌局内随机一个玩家的ID）',
  `UserID` int(11) DEFAULT NULL COMMENT '用户ID',
  `EndIntegral` varchar(1000) DEFAULT NULL COMMENT '格式 ：格式 ：｛Score: 200, freeNum:5}',
  `RecordInfo` varchar(1000) DEFAULT NULL COMMENT '格式：卡片,位字符表示 。如{Cards:"1 2 3;4 5 6;1 2 3;4 5 6;7 8 9;"}',
  `TotalAmount` int(11) DEFAULT NULL COMMENT '用户当期下注总金额',
  `TotalScore` int(11) DEFAULT NULL COMMENT '玩家当期总得分',
  `Gain` int(11) DEFAULT NULL COMMENT '用户当期结算总金额',
  `StartTime` int(11) DEFAULT NULL COMMENT '该局开始时间',
  `EndTime` int(11) DEFAULT NULL COMMENT '该局结束时间',
  `GameTypeID` int(11) DEFAULT NULL COMMENT '最小游戏类型ID',
  `ChannelNO` varchar(50) DEFAULT NULL COMMENT '渠道号',
  PRIMARY KEY (`ID`),
  KEY `StartTime_index` (`StartTime`) USING BTREE,
  KEY `RecordID_index` (`RecordID`) USING BTREE,
  KEY `UserID` (`UserID`),
  KEY `StartTime` (`StartTime`),
  KEY `EndTime` (`EndTime`),
  KEY `StartTime_EndTime` (`StartTime`,`EndTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='水果机牌局记录';


insert into Configure_PlatformOperatorLog values(1101001, 'rank_reward', '比赛排名奖励');
insert into Configure_PlatformOperatorLog values(1201001, 'sign_up_fee', '报名');
insert into Configure_PlatformOperatorLog values(1101002, 'return_sign_up_fee', '取消报名');

CREATE PROCEDURE `PROC_SPLIT_TABLE`(IN `tableName` varchar(64),IN `renameTableName` varchar(64))
BEGIN
	set @split_done = 0;
	SET @check_sql =  CONCAT("SELECT 1 INTO @split_done FROM information_schema.TABLES WHERE TABLE_NAME = '", renameTableName, "';");
	SET @rename_sql = CONCAT("RENAME TABLE ", tableName, " to ", renameTableName, ";");
	SET @create_sql = CONCAT("CREATE TABLE ", tableName, " like ", renameTableName, ";");
	SET @view_sql =   CONCAT("CALL PROC_SYN_VIEW('", tableName, "');");

	PREPARE stmt FROM @check_sql;
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;

	IF ISNULL(@split_done) or @split_done = 0 THEN
		PREPARE stmt1 FROM @rename_sql;   
		EXECUTE stmt1; 
		DEALLOCATE PREPARE stmt1; 

		PREPARE stmt2 FROM @create_sql;   
		EXECUTE stmt2; 
		DEALLOCATE PREPARE stmt2; 

		PREPARE stmt3 FROM @view_sql;   
		EXECUTE stmt3; 
		DEALLOCATE PREPARE stmt3;
	END IF;
END