
-- ----------------------------
-- Table structure for RecordBoard191
-- ----------------------------
DROP TABLE IF EXISTS `RecordBoard191`;
CREATE TABLE `RecordBoard191` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` varchar(50) DEFAULT NULL COMMENT '与RecordUserScorePerGame 的牌局ID相同。生成规则：时间戳（到秒）+最小游戏类型ID（即游戏房间类型）+该局分配的局号（项目组自定义的一个值，例如桌子号等）',
  `Periodical` int(11) DEFAULT NULL COMMENT '开奖的期数',
  `UserID` int(11) DEFAULT NULL COMMENT '用户ID',
  `CardRecord` text COMMENT '记录开奖的牌型记录',
  `BetDetail` text COMMENT '各牌型花色及对应押注、对应牌型得分',
  `TotalAmount` int(11) DEFAULT NULL COMMENT '用户当期下注总金额',
  `TotalScore` int(11) DEFAULT NULL COMMENT '玩家当期总得分',
  `Gain` int(11) DEFAULT NULL COMMENT '用户当期结算总金额',
  `StartTime` int(11) DEFAULT NULL COMMENT '该局开始时间',
  `EndTime` int(11) DEFAULT NULL COMMENT '该局结束时间',
  `GameTypeID` int(11) DEFAULT NULL COMMENT '最小游戏类型ID',
  `ChannelNO` varchar(50) DEFAULT NULL COMMENT '渠道号',
  PRIMARY KEY (`ID`),
  KEY `index_StartTime` (`StartTime`),
  KEY `RecordID` (`RecordID`) USING BTREE,
  KEY `UserID` (`UserID`),
  KEY `GameTypeID` (`GameTypeID`) USING BTREE,
  KEY `StartTime` (`StartTime`),
  KEY `EndTime` (`EndTime`),
  KEY `StartTime_EndTime` (`StartTime`,`EndTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='百家乐牌局记录';

-- ----------------------------
-- Table structure for RedBlackRewardPool
-- ----------------------------
DROP TABLE IF EXISTS `RedBlackRewardPool`;
CREATE TABLE `RedBlackRewardPool` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` varchar(50) DEFAULT NULL COMMENT '与RecordUserScorePerGame 的牌局ID相同。生成规则：时间戳（到秒）+最小游戏类型ID（即游戏房间类型）+该局分配的局号（项目组自定义的一个值，例如桌子号等）',
  `Periodical` int(11) DEFAULT NULL COMMENT '开奖的期数',
  `UserID` int(11) DEFAULT NULL COMMENT '用户ID',
  `betOrder` int(11) DEFAULT NULL COMMENT '名次',
  `beforeCoin` int(11) DEFAULT NULL COMMENT '获奖前金币',
  `reward` int(11) DEFAULT NULL COMMENT '奖金',
  `afterCoin` int(11) DEFAULT NULL COMMENT '获奖后金币',
  `StartTime` int(11) DEFAULT NULL COMMENT '该局开始时间',
  `EndTime` int(11) DEFAULT NULL COMMENT '该局结束时间',
  `GameTypeID` int(11) DEFAULT NULL COMMENT '最小游戏类型ID',
  `ChannelNO` varchar(50) DEFAULT NULL COMMENT '渠道号',
  PRIMARY KEY (`ID`),
  KEY `index_StartTime` (`StartTime`),
  KEY `RecordID` (`RecordID`) USING BTREE,
  KEY `UserID` (`UserID`),
  KEY `GameTypeID` (`GameTypeID`) USING BTREE,
  KEY `StartTime` (`StartTime`),
  KEY `EndTime` (`EndTime`),
  KEY `StartTime_EndTime` (`StartTime`,`EndTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='红黑大战奖金池';


-- ----------------------------
-- Table structure for PlayerSignIn
-- ----------------------------
DROP TABLE IF EXISTS `PlayerSignIn`;
CREATE TABLE `PlayerSignIn` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `accountId` int(11) DEFAULT NULL COMMENT '账户id',
  `todayBet` int(11) DEFAULT NULL COMMENT '签到时的今日下注量(单位分)',
  `dayIndex` int(11) DEFAULT NULL COMMENT '第几天签到',
  `award` int(11) DEFAULT NULL COMMENT '天数奖励(单位分)',
  `additionAward` int(11) DEFAULT NULL COMMENT '额外奖励(单位分)',
  `beforeCoin` int(11) DEFAULT NULL COMMENT '之前金币(单位分)',
  `changeCoin` int(11) DEFAULT NULL COMMENT '金币变化量(单位分)',
  `afterCoin` int(11) DEFAULT NULL COMMENT '之后金币(单位分)',
  `dayArr` blob DEFAULT NULL COMMENT '总共领取奖励的天数',
  `channel` varchar(50) DEFAULT NULL COMMENT '渠道号',
  `loginGuid` varchar(50) DEFAULT NULL COMMENT '登陆关联标识',
  `businessGuid` varchar(50) DEFAULT NULL COMMENT '业务关联标识',
  `recordTime` int(11) DEFAULT NULL COMMENT '记录时间',
  PRIMARY KEY (`ID`),
  KEY `accountId` (`accountId`),
  KEY `channel` (`channel`) USING BTREE,
  KEY `recordTime` (`recordTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='玩家签到记录表';

-- ----------------------------
-- Table structure for PlayerDeposit
-- ----------------------------
DROP TABLE IF EXISTS `PlayerDeposit`;
CREATE TABLE `PlayerDeposit` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `accountId` int(11) DEFAULT NULL COMMENT '账户id',
  `t_orderId` varchar(100) NOT NULL DEFAULT '' COMMENT '充值订单号',
  `rechargeCoin` int(11) DEFAULT NULL COMMENT '充值金额(单位分)',
  `opType` int(11) DEFAULT NULL COMMENT '返利操作类型: 1 充值返利,2 撤销充值扣除返利',
  `rebateCoin` int(11) DEFAULT NULL COMMENT '实际充值返利金币(单位分)',
  `beforeCoin` int(11) DEFAULT NULL COMMENT '返利操作之前金币(单位分)',
  `changeCoin` int(11) DEFAULT NULL COMMENT '金币变化量(单位分)',
  `afterCoin` int(11) DEFAULT NULL COMMENT '返利操作之后金币(单位分)',
  `channel` varchar(50) DEFAULT NULL COMMENT '渠道号',
  `loginGuid` varchar(50) DEFAULT NULL COMMENT '登陆关联标识',
  `businessGuid` varchar(50) DEFAULT NULL COMMENT '业务关联标识',
  `recordTime` int(11) DEFAULT NULL COMMENT '记录时间',
  PRIMARY KEY (`ID`),
  KEY `accountId` (`accountId`),
  KEY `channel` (`channel`) USING BTREE,
  KEY `recordTime` (`recordTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='玩家充值返利记录表';

DROP TABLE IF EXISTS `RecordBoard200`;
CREATE TABLE `RecordBoard200` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` varchar(50) DEFAULT NULL COMMENT '与RecordUserScorePerGame 的牌局ID相同。生成规则：时间戳（到秒）+最小游戏类型ID（即游戏房间类型）+该局分配的局号（项目组自定义的一个值，例如桌子号等）',
  `Periodical` int(11) DEFAULT NULL COMMENT '开奖的期数',
  `UserID` int(11) DEFAULT NULL COMMENT '用户ID',
  `UserType` int(11) DEFAULT NULL COMMENT '用户类型 1-发红包者 2-抢红包者',
  `RedEnvelopeAmount` int(11) DEFAULT NULL COMMENT '红包总金额, 如发10元红包房间, 这里为10元',
  `Tax` int(11) DEFAULT NULL COMMENT '系统抽水, 抢红包者为0, 发红包者为实际抽水',
  `GrabAmount` int(11) DEFAULT NULL COMMENT '玩家抢到红包金额',
  `TotalScore` int(11) DEFAULT NULL COMMENT '如果是发红包者抢了,这里结算为发的-抽水-其他人抢走的; 否则=抢红包金额',
  `IsNext` int(11) DEFAULT NULL COMMENT '是否下一轮发红包者,1-是 0-否',
  `StartTime` int(11) DEFAULT NULL COMMENT '该局开始时间',
  `EndTime` int(11) DEFAULT NULL COMMENT '该局结束时间',
  `GameTypeID` int(11) DEFAULT NULL COMMENT '最小游戏类型ID',
  `ChannelNO` varchar(50) DEFAULT NULL COMMENT '渠道号',
  PRIMARY KEY (`ID`),
  KEY `index_StartTime` (`StartTime`),
  KEY `RecordID` (`RecordID`) USING BTREE,
  KEY `UserID` (`UserID`),
  KEY `GameTypeID` (`GameTypeID`) USING BTREE,
  KEY `StartTime` (`StartTime`),
  KEY `EndTime` (`EndTime`),
  KEY `StartTime_EndTime` (`StartTime`,`EndTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='红包接龙牌局记录';