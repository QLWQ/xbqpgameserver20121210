insert into Configure_PlatformOperatorLog values(2001013, 'invite_first_total_deposit_target', '推广首次累计充值奖励(被邀请人)');
insert into Configure_PlatformOperatorLog values(2001014, 'invite_first_total_deposit_actor', '推广首次累计充值奖励(邀请人)');
insert into Configure_PlatformOperatorLog values(2001015, 'golden_flower_give_up_offline', '炸金花弃牌后退出、掉线');
insert into Configure_PlatformOperatorLog values(2001016, 'recharge_web_hand_donate', '手动赠送');

ALTER TABLE `UserTreasureChange` ADD INDEX `UserType`(`UserType`);
ALTER TABLE `UserTreasureChange` ADD INDEX `GameTypeID`(`GameTypeID`);
ALTER TABLE `UserTreasureChange` ADD INDEX `BusinessType`(`BusinessType`);

