-- ----------------------------
-- Table structure for RecordDamaChange
-- ----------------------------
DROP TABLE IF EXISTS `RecordDamaChange`;
CREATE TABLE `RecordDamaChange` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) DEFAULT NULL COMMENT '用户ID',
  `BeforeAmount` decimal(18,0) DEFAULT NULL COMMENT '当前打码量前值',
  `ChangeAmount` decimal(18,0) DEFAULT NULL COMMENT '当前打码量变化值',
  `AfterAmount` decimal(18,0) DEFAULT NULL COMMENT '当前打码量后值',
  `BeforeNeed` decimal(18,0) DEFAULT NULL COMMENT '出款所需打码量前值',
  `ChangeNeed` decimal(18,0) DEFAULT NULL COMMENT '出款所需打码量变化值',
  `AfterNeed` decimal(18,0) DEFAULT NULL COMMENT '出款所需打码量后值',
  `Type` int(11) DEFAULT NULL COMMENT '变化类型 1-充值 2-人工增加 3-人工扣除 4-真人投注',
  `RecordTime` int(11) DEFAULT NULL COMMENT '操作时间',
  `ChannelNO` varchar(50) DEFAULT NULL COMMENT '渠道号',
  PRIMARY KEY (`ID`),
  KEY `UserID` (`UserID`),
  KEY `RecordTime` (`RecordTime`),
  KEY `ChannelNO` (`ChannelNO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

