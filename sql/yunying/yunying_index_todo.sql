
ALTER TABLE AiTreasureChange ADD INDEX `UserID` (`UserID`);
ALTER TABLE RechargePlatformOrder ADD INDEX `UserID` (`UserID`);
ALTER TABLE RecordAILoginGame ADD INDEX `UserID` (`UserID`);
ALTER TABLE RecordAILogoutGame ADD INDEX `UserID` (`UserID`);

ALTER TABLE RecordBoard106 MODIFY COLUMN UserID CHAR(30);
ALTER TABLE RecordBoard106 ADD INDEX `UserID` (`UserID`);
ALTER TABLE RecordBoard141 ADD INDEX `UserID` (`UserID`);
ALTER TABLE RecordBoard151 ADD INDEX `UserID` (`UserID`);

ALTER TABLE RecordBoard160 ADD INDEX `index_StartTime` (`StartTime`);
ALTER TABLE RecordBoard160 ADD INDEX `RecordID` (`RecordID`);
ALTER TABLE RecordBoard160 ADD INDEX `Userid` (`Userid`);
ALTER TABLE RecordBoard160 ADD INDEX `GameTypeID` (`GameTypeID`);
ALTER TABLE RecordBoard160 ADD INDEX `StartTime` (`StartTime`);
ALTER TABLE RecordBoard160 ADD INDEX `EndTime` (`EndTime`);
ALTER TABLE RecordBoard160 ADD INDEX `StartTime_EndTime` (`StartTime`,`EndTime`);

ALTER TABLE RecordBoard161 ADD INDEX `UserID` (`UserID`);
ALTER TABLE RecordBoard181 ADD INDEX `UserID` (`UserID`);
ALTER TABLE RecordExtInfo101 ADD INDEX `UserID` (`UserID`);
ALTER TABLE RecordExtInfo102 ADD INDEX `UserID` (`UserID`);

ALTER TABLE RecordExtInfo171 ADD INDEX `UserID` (`UserID`);
ALTER TABLE RecordUserDailyRank ADD INDEX `UserID` (`UserID`);
ALTER TABLE RecordUserMonthlyRank ADD INDEX `UserID` (`UserID`);

insert into Configure_PlatformOperatorLog values(1101006,'open_reward', '奖金池派奖');
insert into Configure_PlatformOperatorLog values(1001003,'red_env_give', '发红包');
insert into Configure_PlatformOperatorLog values(1001004,'red_env_return', '红包退还');
insert into Configure_PlatformOperatorLog values(2001007,'player_sign_in_award', '签到奖励');
insert into Configure_PlatformOperatorLog values(2001008,'player_deposit_rebate', '充值返利');
insert into Configure_PlatformOperatorLog values(2001009,'cancel_deposit_rebate', '撤销充值导致的扣减充值返利');