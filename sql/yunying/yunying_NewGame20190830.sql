-- ----------------------------
-- Table structure for RecordBoard201
-- ----------------------------
DROP TABLE IF EXISTS `RecordBoard201`;
CREATE TABLE `RecordBoard201` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` varchar(50) DEFAULT NULL COMMENT '与RecordUserScorePerGame 的牌局ID相同。生成规则：时间戳（到秒）+最小游戏类型ID（即游戏房间类型）+该局分配的局号（项目组自定义的一个值，例如桌子号等）',
  `gameObjId` varchar(50) DEFAULT NULL COMMENT '全局唯一桌子号',
  `ante` int(11) DEFAULT NULL COMMENT '底注(服务器金币)',
  `minEnterCoin` int(11) DEFAULT NULL COMMENT '入场金币限制(服务器金币)',
  `playerCnt` int(11) DEFAULT NULL COMMENT '本桌玩家人数',
  `bankerId` int(11) DEFAULT NULL COMMENT '庄家账户id',
  `bankerScore` int(11) DEFAULT NULL COMMENT '庄家抢分,  1:抢1  2：抢2  3：抢3  4：抢4',
  `betMultiple` text DEFAULT NULL COMMENT '闲家下注倍数',
  `CardRecord` text DEFAULT NULL COMMENT '开牌记录',
  `cardType` text DEFAULT NULL COMMENT '牌型',
  `winType` int(11) DEFAULT NULL COMMENT '输赢类型,1:庄家胜   2: 闲家胜(UserID==bankerId 时,忽略此项数值)',
  `winMultiple` int(11) DEFAULT NULL COMMENT '获胜方牌型倍数,获胜方牌型倍数(UserID==bankerId 时,忽略此项数值)',
  `UserID` int(11) DEFAULT NULL COMMENT '玩家ID',
  `role` int(11) DEFAULT NULL COMMENT '玩家扮演角色,0:闲家  1:庄家',
  `isAi` int(11) DEFAULT NULL COMMENT '是否AI,0:否  1:是',
  `nickname` varchar(50) DEFAULT NULL COMMENT '玩家昵称',
  `beforeCoin` int(11) DEFAULT NULL COMMENT '之前金币(服务器金币)',
  `changeCoin` int(11) DEFAULT NULL COMMENT '金币变化量(服务器金币)',
  `afterCoin` int(11) DEFAULT NULL COMMENT '之后金币(服务器金币)',
  `tax` int(11) DEFAULT NULL COMMENT '税收(服务器金币)',
  `StartTime` int(11) DEFAULT NULL COMMENT '该局开始时间',
  `EndTime` int(11) DEFAULT NULL COMMENT '该局结束时间',
  `GameTypeID` int(11) DEFAULT NULL COMMENT '最小游戏类型ID',
  `ChannelNO` varchar(50) DEFAULT NULL COMMENT '渠道号',
  PRIMARY KEY (`ID`),
  KEY `index_StartTime` (`StartTime`),
  KEY `RecordID` (`RecordID`) USING BTREE,
  KEY `UserID` (`UserID`),
  KEY `playerCnt`(`playerCnt`),
  KEY `GameTypeID` (`GameTypeID`) USING BTREE,
  KEY `ChannelNO`(`ChannelNO`),
  KEY `StartTime` (`StartTime`),
  KEY `EndTime` (`EndTime`),
  KEY `StartTime_EndTime` (`StartTime`,`EndTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='抢庄牛牛牌局记录';


-- ----------------------------
-- Table structure for RecordBoard203
-- ----------------------------
DROP TABLE IF EXISTS `RecordBoard203`;
CREATE TABLE `RecordBoard203` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` varchar(50) DEFAULT NULL COMMENT '与RecordUserScorePerGame 的牌局ID相同。生成规则：时间戳（到秒）+最小游戏类型ID（即游戏房间类型）+该局分配的局号（项目组自定义的一个值，例如桌子号等）',
  `minGameCoin` int(11) DEFAULT NULL COMMENT '最低游戏金币(服务器金币)',
  `UserID` int(11) DEFAULT NULL COMMENT '玩家ID',
  `isAi` int(11) DEFAULT NULL COMMENT '是否AI,0:否  1:是',
  `nickname` varchar(50) DEFAULT NULL COMMENT '玩家昵称',
  `rewardType` int(11) DEFAULT NULL COMMENT '开奖类型',
  `gridId` int(11) DEFAULT NULL COMMENT '开奖格子id',
  `awardBetArea` text DEFAULT NULL COMMENT '中奖下注区域',
  `areaTimes` text DEFAULT NULL COMMENT '各下注区域倍数',
  `goldShark` int(11) DEFAULT NULL COMMENT '金鲨倍数',
  `sliverShark` int(11) DEFAULT NULL COMMENT '银鲨倍数',
  `playerBet` text DEFAULT NULL COMMENT '玩家下注(服务器金币)',
  `totalProfit` int(11) DEFAULT NULL COMMENT '玩家总盈利(服务器金币)',
  `beforeCoin` int(11) DEFAULT NULL COMMENT '之前金币(服务器金币)',
  `changeCoin` int(11) DEFAULT NULL COMMENT '金币变化量(服务器金币)',
  `afterCoin` int(11) DEFAULT NULL COMMENT '之后金币(服务器金币)',
  `tax` int(11) DEFAULT NULL COMMENT '税收(服务器金币)',
  `taxRate` int(11) DEFAULT NULL COMMENT '税收比例(千分比的分子)',
  `StartTime` int(11) DEFAULT NULL COMMENT '该局开始时间',
  `EndTime` int(11) DEFAULT NULL COMMENT '该局结束时间',
  `GameTypeID` int(11) DEFAULT NULL COMMENT '最小游戏类型ID',
  `ChannelNO` varchar(50) DEFAULT NULL COMMENT '渠道号',
  PRIMARY KEY (`ID`),
  KEY `index_StartTime` (`StartTime`),
  KEY `RecordID` (`RecordID`) USING BTREE,
  KEY `UserID` (`UserID`),
  KEY `rewardType`(`rewardType`),
  KEY `gridId`(`gridId`),
  KEY `GameTypeID` (`GameTypeID`) USING BTREE,
  KEY `ChannelNO`(`ChannelNO`),
  KEY `StartTime` (`StartTime`),
  KEY `EndTime` (`EndTime`),
  KEY `StartTime_EndTime` (`StartTime`,`EndTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='飞禽走兽牌局记录';

-- ----------------------------
-- Table structure for RecordBoard204
-- ----------------------------
DROP TABLE IF EXISTS `RecordBoard204`;
CREATE TABLE `RecordBoard204` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` varchar(50) DEFAULT NULL COMMENT '与RecordUserScorePerGame 的牌局ID相同。生成规则：时间戳（到秒）+最小游戏类型ID（即游戏房间类型）+该局分配的局号（项目组自定义的一个值，例如桌子号等）',
  `minGameCoin` int(11) DEFAULT NULL COMMENT '最低游戏金币(服务器金币)',
  `UserID` int(11) DEFAULT NULL COMMENT '玩家ID',
  `isAi` int(11) DEFAULT NULL COMMENT '是否AI,0:否  1:是',
  `nickname` varchar(50) DEFAULT NULL COMMENT '玩家昵称',
  `rewardType` int(11) DEFAULT NULL COMMENT '开奖类型',
  `gridId` int(11) DEFAULT NULL COMMENT '开奖格子id',
  `awardBetArea` text DEFAULT NULL COMMENT '中奖下注区域',
  `areaTimes` text DEFAULT NULL COMMENT '各下注区域倍数',
  `playerBet` text DEFAULT NULL COMMENT '玩家下注(服务器金币)',
  `totalProfit` int(11) DEFAULT NULL COMMENT '玩家总盈利(服务器金币)',
  `beforeCoin` int(11) DEFAULT NULL COMMENT '之前金币(服务器金币)',
  `changeCoin` int(11) DEFAULT NULL COMMENT '金币变化量(服务器金币)',
  `afterCoin` int(11) DEFAULT NULL COMMENT '之后金币(服务器金币)',
  `tax` int(11) DEFAULT NULL COMMENT '税收(服务器金币)',
  `taxRate` int(11) DEFAULT NULL COMMENT '税收比例(千分比的分子)',
  `StartTime` int(11) DEFAULT NULL COMMENT '该局开始时间',
  `EndTime` int(11) DEFAULT NULL COMMENT '该局结束时间',
  `GameTypeID` int(11) DEFAULT NULL COMMENT '最小游戏类型ID',
  `ChannelNO` varchar(50) DEFAULT NULL COMMENT '渠道号',
  PRIMARY KEY (`ID`),
  KEY `index_StartTime` (`StartTime`),
  KEY `RecordID` (`RecordID`) USING BTREE,
  KEY `UserID` (`UserID`),
  KEY `rewardType`(`rewardType`),
  KEY `gridId`(`gridId`),
  KEY `GameTypeID` (`GameTypeID`) USING BTREE,
  KEY `ChannelNO`(`ChannelNO`),
  KEY `StartTime` (`StartTime`),
  KEY `EndTime` (`EndTime`),
  KEY `StartTime_EndTime` (`StartTime`,`EndTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='奔驰宝马牌局记录';