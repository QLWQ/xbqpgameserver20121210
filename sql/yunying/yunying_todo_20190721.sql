ALTER TABLE Configure_PlatformRecharge ADD INDEX `channelNum` (`channelNum`);
ALTER TABLE PlatFormOnlineCount ADD INDEX `ChannelNO` (`ChannelNO`);

ALTER TABLE RecordBoard101 ADD INDEX `GameTypeID` (`GameTypeID`);


ALTER TABLE RecordBoard105 ADD INDEX `ChannelNO` (`ChannelNO`);
ALTER TABLE RecordBoard106 ADD INDEX `ChannelNO` (`ChannelNO`);
ALTER TABLE RecordBoard141 ADD INDEX `ChannelNO` (`ChannelNO`);
ALTER TABLE RecordBoard141 ADD INDEX `Periodical` (`Periodical`);
ALTER TABLE RecordBoard151 ADD INDEX `ChannelNO` (`ChannelNO`);
ALTER TABLE RecordBoard151 ADD INDEX `Periodical` (`Periodical`);
ALTER TABLE RecordBoard161 ADD INDEX `ChannelNO` (`ChannelNO`);
ALTER TABLE RecordBoard161 ADD INDEX `Periodical` (`Periodical`);
ALTER TABLE RecordBoard171 ADD INDEX `GameTypeID` (`GameTypeID`);
ALTER TABLE RecordBoard181 ADD INDEX `ChannelNO` (`ChannelNO`);
ALTER TABLE RecordBoard181 ADD INDEX `GameTypeID` (`GameTypeID`);
ALTER TABLE RecordBoard191 ADD INDEX `ChannelNO` (`ChannelNO`);
ALTER TABLE RecordBoard191 ADD INDEX `Periodical` (`Periodical`);
ALTER TABLE RecordBoard200 ADD INDEX `ChannelNO` (`ChannelNO`);
ALTER TABLE RecordBoard200 ADD INDEX `Periodical` (`Periodical`);
ALTER TABLE RecordPlayerDrop ADD INDEX `GameTypeID` (`GameTypeID`);
ALTER TABLE RecordPlayerReconnect ADD INDEX `GameTypeID` (`GameTypeID`);
ALTER TABLE RecordUserMailSend ADD INDEX `GameTypeID` (`GameTypeID`);
ALTER TABLE RedBlackRewardPool ADD INDEX `ChannelNO` (`ChannelNO`);
ALTER TABLE RedBlackRewardPool ADD INDEX `Periodical` (`Periodical`);
ALTER TABLE RedBlackRewardPool ADD INDEX `betOrder` (`betOrder`);
ALTER TABLE PlayerSignIn ADD INDEX `dayIndex` (`dayIndex`);
ALTER TABLE PlayerDeposit ADD INDEX `t_orderId` (`t_orderId`);
ALTER TABLE PlayerDeposit ADD INDEX `opType` (`opType`);
