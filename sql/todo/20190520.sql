DROP TABLE IF EXISTS `t_onlinePlayerList`;
CREATE TABLE `t_onlinePlayerList` (
	`t_accountID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '玩家id',
	`t_gameId` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '游戏ID',
	`t_nickName` varchar(30) NOT NULL DEFAULT '' COMMENT '昵称',
	`t_enterTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '进入时间',
	`t_coin` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '当前金币',
	`t_channel` varchar(30) NOT NULL DEFAULT '' COMMENT '渠道',
	PRIMARY KEY (`t_accountID` , `t_gameId`),
	KEY `t_gameId` (`t_gameId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

ALTER TABLE t_userGameData ADD INDEX userGameData_game_idx(t_gameId);