DROP TABLE IF EXISTS `t_rank`;
CREATE TABLE `t_rank` (
	`t_accountID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '玩家id',
	`t_nickName` varchar(30) NOT NULL DEFAULT '' COMMENT '昵称',
	`t_avaterId` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '头像',
	`t_frameId` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '头像框',
	`t_vipLevel` int UNSIGNED NOT NULL DEFAULT 0 COMMENT 'vip等级',
	`t_channelId` varchar(16) NOT NULL DEFAULT '' COMMENT '渠道ID',
	`t_strJsonRank` blob   COMMENT '排名详细数据',
	PRIMARY KEY (`t_accountID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_ddzMatchRankAward`;
CREATE TABLE `t_ddzMatchRankAward` (
	`t_channel` varchar(64) NOT NULL DEFAULT '' COMMENT '渠道ID',
	`t_option` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '榜单类型 1-日榜 2-周榜',
	`t_openType` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '开关类型',
	`t_strJsonAward` blob   COMMENT '比赛排名额外奖励',
	`t_cfgTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '配置时间',
	PRIMARY KEY (`t_channel` , `t_option`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_editNickAward`;
CREATE TABLE `t_editNickAward` (
	`t_channel` varchar(64) NOT NULL DEFAULT '' COMMENT '渠道ID',
	`t_openType` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '开关类型',
	`t_strJsonAward` blob   COMMENT '修改昵称额外信息',
	`t_cfgTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '配置时间',
	PRIMARY KEY (`t_channel`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

alter table t_accountInfo add column `t_origin` varchar(512)  DEFAULT '' COMMENT '注册来源'  AFTER `t_bankInfo`;