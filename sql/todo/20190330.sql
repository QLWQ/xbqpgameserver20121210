
DROP TABLE IF EXISTS `t_offlineOrder`;
CREATE TABLE `t_offlineOrder` (
	`t_order` varchar(64) NOT NULL  COMMENT '订单ID',
	`t_accountID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '玩家id',
	`t_type` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '离线订单类型 1-审核兑换 2-直接兑换 3-充值',
	`t_extInfo` varchar(1024) NOT NULL DEFAULT '' COMMENT '离线订单详情',
	`t_recordTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '记录时间',
	PRIMARY KEY (`t_order`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;