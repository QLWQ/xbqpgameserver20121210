DROP TABLE IF EXISTS `t_gameCtrl`;
CREATE TABLE `t_gameCtrl` (
	`t_gameId` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '游戏ID',
	`t_extend` blob NOT NULL  COMMENT '控制信息',
	PRIMARY KEY (`t_gameId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

-- alter table t_userBaseAttr add column `t_gameCtrlData` blob   COMMENT '玩家游戏控制数据';

DROP TABLE IF EXISTS `t_userGameData`;
CREATE TABLE `t_userGameData` (
	`t_accountID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
	`t_gameId` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '游戏ID',
	`t_totalRound` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '该游戏玩的总局数',
	`t_histWin` int NOT NULL DEFAULT 0 COMMENT '该游戏历史总输赢',
	`t_todayWin` int NOT NULL DEFAULT 0 COMMENT '该游戏今日总输赢',
	`t_todayRsfTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '该游戏玩今日刷新时间',
	`t_totalPlayCount` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '游戏次数, 进去出来算一次',
	`t_firstEnterTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '玩家第一次游戏时间',
	`t_lastEnterTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '玩家最后一次游戏时间',
	`t_extend` blob   COMMENT '玩家游戏扩展数据, 非常用数据',
	`t_type` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '控制类型 0-不控制 1-放水 2-缩水',
	`t_setTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '设控时间',
	`t_betLimit` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '下注金额限制, 超过该值放水, 小于该值缩水',
	`t_round` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '控制局数',
	`t_rate` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '控制触发概率',
	`t_ctrlRound` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '已控局数',
	`t_ctlScore` int NOT NULL DEFAULT 0 COMMENT '放水量/收水量(针对被控局的统计)',
	`t_curScore` int NOT NULL DEFAULT 0 COMMENT '游戏当前金币',
	`t_channel` varchar(64) NOT NULL DEFAULT '' COMMENT '渠道ID',
	PRIMARY KEY (`t_accountID` , `t_gameId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

alter table t_userBaseAttr drop column t_avatarFrameID, drop column t_tryAvatarID, drop column t_auditingAvatarID;
alter table t_userBaseAttr drop column t_customMaxAvatarID, drop column t_unlockSystemAvatarList, drop column t_customAvatarList;
alter table t_userBaseAttr drop column t_gameHistory, drop column t_gameExData;