DROP TABLE IF EXISTS `t_inviteFirstTotalDeposit`;
CREATE TABLE `t_inviteFirstTotalDeposit` (
	`t_channel` varchar(64) NOT NULL DEFAULT '' COMMENT '渠道ID',
	`t_openType` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '开关类型',
	`t_timeType` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '时间类型',
	`t_beginTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '活动开始时间',
	`t_endTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '活动结束时间',
	`t_firstTotalDeposit` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '首次累计充值达标金额',
	`t_selfReward` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '给自己奖励金币值',
	`t_upperReward` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '给直接上级奖励金币值',
	`t_cfgTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '配置时间',
	PRIMARY KEY (`t_channel`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `t_hallGameResult`;
CREATE TABLE `t_hallGameResult` (
	`t_key` varchar(64) NOT NULL DEFAULT '' COMMENT '唯一key',
	`t_gameResultInfo` blob NOT NULL  COMMENT '战绩信息',
	PRIMARY KEY (`t_key`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `t_onlinePlayerList`;
CREATE TABLE `t_onlinePlayerList` (
	`t_accountID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '玩家id',
	`t_gameId` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '游戏ID',
	`t_nickName` varchar(30) NOT NULL DEFAULT '' COMMENT '昵称',
	`t_enterTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '进入时间',
	`t_coin` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '当前金币',
	`t_channel` varchar(30) NOT NULL DEFAULT '' COMMENT '渠道',
	PRIMARY KEY (`t_accountID`),
	KEY `t_accountID` (`t_accountID`),KEY `t_gameId` (`t_gameId`),KEY `t_enterTime` (`t_enterTime`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

alter table t_userBaseAttr add column `t_lastFirstTotalDeposit` 	int UNSIGNED NOT NULL DEFAULT 0 COMMENT '上一次首次累计充值';
alter table t_userBaseAttr add column `t_lastFirstTotalDepositTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '上一次首次累计充值时间';
alter table t_userBaseAttr add column `t_isReciveFirstTotalDeposit` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否领取首次累计充值的奖励';

alter table t_luckyRoulette add column `t_openType` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '开关类型';