
alter table t_accountInfo add column `t_realName` varchar(512)  DEFAULT '' COMMENT '注册来源'  AFTER `t_origin`;
alter table t_accountInfo add column `t_deviceToken` varchar(512)  DEFAULT '' COMMENT '设备token'  AFTER `t_realName`;