DROP TABLE IF EXISTS `t_userMail`;
CREATE TABLE `t_userMail` (
	`t_id` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '邮件ID',
	`t_accountID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
	`t_title` varchar(128) NOT NULL DEFAULT '' COMMENT '邮件标题',
	`t_content` blob NOT NULL  COMMENT '邮件内容',
	`t_rewardList` varchar(1024) NOT NULL DEFAULT '' COMMENT '邮件附件奖励列表',
	`t_sender` varchar(50) NOT NULL DEFAULT '' COMMENT '发送人',
	`t_state` smallint UNSIGNED NOT NULL DEFAULT 0 COMMENT '邮件状态 1-未读 2-已读 3-领取',
	`t_isTextMail` smallint UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否文本邮件 0-否 1-是',
	`t_isGroupMail` smallint UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否群发邮件 0-否 1-是',
	`t_isTemplateMail` smallint UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否模板邮件 0-否 1-是',
	`t_templateMailInfo` varchar(1024) NOT NULL DEFAULT '' COMMENT '模板邮件参数',
	`t_sendDate` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '发送日期',
	`t_recvDate` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '接收日期',
	`t_triggerGameType` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '触发邮件游戏类型',
	`t_businessType` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '业务类型',
	`t_businessGuid` varchar(50) NOT NULL DEFAULT '' COMMENT '业务关联标识',
	PRIMARY KEY (`t_id` , `t_accountID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_userTempmail`;
CREATE TABLE `t_userTempmail` (
	`t_id` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '邮件ID',
	`t_accountID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
	`t_title` varchar(128) NOT NULL DEFAULT '' COMMENT '邮件标题',
	`t_content` blob NOT NULL  COMMENT '邮件内容',
	`t_rewardList` varchar(1024) NOT NULL DEFAULT '' COMMENT '邮件附件奖励列表',
	`t_sender` varchar(50) NOT NULL DEFAULT '' COMMENT '发送人',
	`t_state` smallint UNSIGNED NOT NULL DEFAULT 0 COMMENT '邮件状态 1-未读 2-已读 3-领取',
	`t_isTextMail` smallint UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否文本邮件 0-否 1-是',
	`t_isGroupMail` smallint UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否群发邮件 0-否 1-是',
	`t_isTemplateMail` smallint UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否模板邮件 0-否 1-是',
	`t_templateMailInfo` varchar(1024) NOT NULL DEFAULT '' COMMENT '模板邮件参数',
	`t_sendDate` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '发送日期',
	`t_triggerGameType` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '触发邮件游戏类型',
	`t_businessType` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '业务类型',
	`t_businessGuid` varchar(50) NOT NULL DEFAULT '' COMMENT '业务关联标识',
	PRIMARY KEY (`t_id` , `t_accountID`),
	KEY `t_id` (`t_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_groupMail`;
CREATE TABLE `t_groupMail` (
	`t_id` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '邮件ID',
	`t_title` varchar(128) NOT NULL DEFAULT '' COMMENT '邮件标题',
	`t_content` blob NOT NULL  COMMENT '邮件内容',
	`t_rewardList` varchar(1024) NOT NULL DEFAULT '' COMMENT '邮件附件奖励列表',
	`t_sender` varchar(50) NOT NULL DEFAULT '' COMMENT '发送人',
	`t_isTextMail` smallint UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否文本邮件 0-否 1-是',
	`t_isGroupMail` smallint UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否群发邮件 0-否 1-是',
	`t_isTemplateMail` smallint UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否模板邮件 0-否 1-是',
	`t_templateMailInfo` varchar(1024) NOT NULL DEFAULT '' COMMENT '模板邮件参数',
	`t_sendDate` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '发送日期',
	`t_toUserList` blob NOT NULL  COMMENT '目标发送玩家列表',
	`t_sendType` smallint UNSIGNED NOT NULL DEFAULT 0 COMMENT '发送类型',
	`t_productIDList` varchar(1024) NOT NULL DEFAULT '' COMMENT '发送产品id列表',
	`t_state` smallint UNSIGNED NOT NULL DEFAULT 0 COMMENT '邮件状态 1-未读 2-已读 3-领取',
	`t_triggerGameType` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '触发邮件游戏类型',
	`t_businessType` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '业务类型',
	`t_businessGuid` varchar(50) NOT NULL DEFAULT '' COMMENT '业务关联标识',
	PRIMARY KEY (`t_id`),
	KEY `t_id` (`t_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_userBaseAttr`;
CREATE TABLE `t_userBaseAttr` (
	`t_accountID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
	`t_sex` smallint UNSIGNED NOT NULL DEFAULT 0 COMMENT '性别 0保密 1男 2女',
	`t_vipLevel` smallint UNSIGNED NOT NULL DEFAULT 1 COMMENT 'vip等级',
	`t_goldCoin` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '金币',
	`t_avatarID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '头像id',
	`t_avatarFrameID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '头像框id',
	`t_tryAvatarID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '试用头像id',
	`t_auditingAvatarID` int UNSIGNED  DEFAULT 0 COMMENT '审核头像id',
	`t_customMaxAvatarID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '自定义最大头像id',
	`t_unlockSystemAvatarList` varchar(150)  DEFAULT '' COMMENT '解锁系统头像id列表',
	`t_customAvatarList` varchar(150)  DEFAULT '' COMMENT '自定义头像id列表',
	`t_bankPassword` varchar(64) NOT NULL DEFAULT '' COMMENT '银行密码',
	`t_bankGoldCoin` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '银行金币数',
	`t_isModifiedNick` smallint UNSIGNED NOT NULL DEFAULT 0 COMMENT '昵称是否被修改过',
	`t_moduleScatteredData` blob   COMMENT '各个模块零散数据集合',
	`t_totalGameTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '总计累计游戏时长',
	`t_todayGameTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '今日累计游戏时长',
	`t_todayGameTimeRefreshTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '今日游戏时长刷新时间',
	`t_totalCoinGet` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '总计游戏赢取金币数',
	`t_todayCoinGet` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '今日输赢金币数',
	`t_todayCoinRefreshTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '今日赢金币数刷新时间',
	`t_rechargeMoney` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '累计充值金额',
	`t_gameInfo` blob   COMMENT '玩家游戏携带数据',
	`t_gameHistory` blob   COMMENT '玩家游戏历史数据,统计各游戏所玩次数，最后所玩游戏',
	`t_gameExData` blob   COMMENT '玩家游戏扩展数据',
	PRIMARY KEY (`t_accountID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_accountInfo`;
CREATE TABLE `t_accountInfo` (
	`t_accountID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
	`t_accountName` varchar(30) NOT NULL DEFAULT '' COMMENT '账户名',
	`t_phoneNumber` varchar(50) NOT NULL DEFAULT '' COMMENT '绑定手机号',
	`t_channelKey` varchar(50) NOT NULL DEFAULT '' COMMENT '渠道key',
	`t_accountKey` varchar(50) NOT NULL DEFAULT '' COMMENT '账户密码',
	`t_nickName` varchar(30) NOT NULL DEFAULT '' COMMENT '昵称',
	`t_bIsRegularAccount` smallint UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否正式账号 0-否 1-是',
	`t_idCard` varchar(30) NOT NULL DEFAULT '' COMMENT '绑定身份证',
	`t_lockMachine` varchar(150)  DEFAULT '' COMMENT '锁定机器Json格式存储',
	`t_lastLoginTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '最后一次登入时间',
	`t_lastLogoutTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '最后一次登出时间',
	`t_commonDevices` varchar(1024)  DEFAULT '' COMMENT '常用设备集合',
	`t_commonIPs` varchar(1024)  DEFAULT '' COMMENT '常登陆IP集合',
	`t_accountState` smallint UNSIGNED NOT NULL DEFAULT 0 COMMENT '账号状态 0-正常 1-临时冻结（异常验证失败导致） 2-永久冻结',
	`t_authPwdFailTimes` smallint UNSIGNED NOT NULL DEFAULT 0 COMMENT '密码验证错误次数',
	`t_excepVerifyInfo` varchar(1024)  DEFAULT '' COMMENT '异常登录验证进度 json格式',
	`t_regTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '注册时间',
	`t_regTerminalType` smallint UNSIGNED NOT NULL DEFAULT 1 COMMENT '注册终端 1-mobile',
	`t_regIP` varchar(30) NOT NULL DEFAULT '' COMMENT '注册IP',
	`t_regDevice` varchar(50) NOT NULL DEFAULT '' COMMENT '注册设备',
	`t_isUniqDeviceCode` smallint UNSIGNED NOT NULL DEFAULT 0 COMMENT '机器码注册时候是否唯一 0:不唯一  1：唯一 ',
	`t_shareType` smallint UNSIGNED NOT NULL DEFAULT 0 COMMENT '分享方式',
	`t_shareTriggerFuncId` smallint UNSIGNED NOT NULL DEFAULT 0 COMMENT '触发分享功能id',
	`t_alipayInfo` varchar(128)  DEFAULT '' COMMENT '支付宝账号信息',
	`t_bankInfo` varchar(256)  DEFAULT '' COMMENT '银行账号信息',
	PRIMARY KEY (`t_accountID`),
	KEY `t_accountName` (`t_accountName`),KEY `t_phoneNumber` (`t_phoneNumber`),KEY `t_channelKey` (`t_channelKey`),KEY `t_nickName` (`t_nickName`),KEY `t_idCard` (`t_idCard`),KEY `t_regDevice` (`t_regDevice`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_bag`;
CREATE TABLE `t_bag` (
	`t_gridId` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '格子ID',
	`t_accountID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
	`t_itemId` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '物品ID',
	`t_count` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '物品数量',
	`t_extendParams` blob NOT NULL  COMMENT '拓展参数',
	PRIMARY KEY (`t_gridId` , `t_accountID`),
	KEY `t_accountID` (`t_accountID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_bulletin`;
CREATE TABLE `t_bulletin` (
	`t_id` varchar(150) NOT NULL DEFAULT '' COMMENT '公告ID',
	`t_type` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '公告类型 1-公告栏 2-跑马灯',
	`t_title` varchar(150) NOT NULL DEFAULT '' COMMENT '公告标题',
	`t_content` blob NOT NULL  COMMENT '公告内容, 其中包括公告作用时间, 内容',
	`t_hyperLink` varchar(256) NOT NULL DEFAULT '' COMMENT '公告超链接,没有则为空,指向图片等url地址',
	`t_start` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '公告开始时间',
	`t_finish` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '公告结束时间',
	`t_channelList` blob NOT NULL  COMMENT '影响渠道,json格式,记录所有渠道, 如果针对所有渠道这里为空',
	`t_createTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
	PRIMARY KEY (`t_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_bulletin2Channel`;
CREATE TABLE `t_bulletin2Channel` (
	`t_bulletinId` varchar(150) NOT NULL DEFAULT '' COMMENT '公告ID',
	`t_channelId` varchar(16) NOT NULL DEFAULT '' COMMENT '如果为空表示针对所有渠道',
	PRIMARY KEY (`t_bulletinId` , `t_channelId`),
	KEY `t_bulletinId` (`t_bulletinId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_bagExchangeInfo`;
CREATE TABLE `t_bagExchangeInfo` (
	`t_accountId` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '玩家ID',
	`t_dailyExchangeGold` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '当日累计兑换成金币数量',
	`t_exchangeGoldTotal` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '兑换成金币总量',
	`t_exchangeGoldTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '上次兑换成金币时间',
	`t_dailyExchangeNum` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '当日兑换话费/流量金额， 一元话费卡记录为1元',
	`t_totalExchangeNum` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '历史累计兑换话费/流量金额总金额',
	`t_exchangeTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '上次兑换流量时间',
	`t_dailyExchangeCount` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '当日累计兑换话费/流量次数',
	`t_index` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '索引',
	PRIMARY KEY (`t_accountId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_bagExchangeOrder`;
CREATE TABLE `t_bagExchangeOrder` (
	`t_orderId` varchar(150) NOT NULL DEFAULT '' COMMENT '订单ID',
	`t_accountId` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '玩家ID',
	`t_time` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '订单发起时间',
	`t_status` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '订单状态',
	`t_type` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '订单类型',
	`t_num` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '兑换的数量',
	`t_cost` varchar(150) NOT NULL DEFAULT '' COMMENT '消耗物品',
	PRIMARY KEY (`t_orderId`),
	KEY `t_orderId` (`t_orderId`),KEY `t_accountId` (`t_accountId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_channelSwitchSetting`;
CREATE TABLE `t_channelSwitchSetting` (
	`t_channelKey` varchar(16) NOT NULL DEFAULT '' COMMENT '渠道号',
	`t_channelName` varchar(128) NOT NULL DEFAULT '' COMMENT '渠道名',
	`t_versionNo` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '设置版本号',
	`t_openList` blob NOT NULL  COMMENT '开放游戏列表',
	`t_extend` blob NOT NULL  COMMENT '其他扩展信息, 包括是否开放兑换,推广返利第一级比例等后面再扩展',
	PRIMARY KEY (`t_channelKey`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_productSwitchSetting`;
CREATE TABLE `t_productSwitchSetting` (
	`t_productId` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '产品ID',
	`t_setting` blob NOT NULL  COMMENT '开放功能id列表',
	PRIMARY KEY (`t_productId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_recharge_order`;
CREATE TABLE `t_recharge_order` (
	`t_orderId` varchar(100) NOT NULL DEFAULT '' COMMENT '订单号',
	`t_accountId` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '玩家ID',
	`t_type` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '货币类型',
	`t_amount` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '货币数量',
	`t_createTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
	`t_status` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '当前状态',
	PRIMARY KEY (`t_orderId`),
	KEY `t_accountId` (`t_accountId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_share_info`;
CREATE TABLE `t_share_info` (
	`t_accountId` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '玩家ID',
	`t_shareInfo` blob NOT NULL  COMMENT '分享数据',
	PRIMARY KEY (`t_accountId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_fireWall`;
CREATE TABLE `t_fireWall` (
	`t_accountId` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '玩家ID',
	`t_accountName` varchar(50) NOT NULL DEFAULT '' COMMENT '玩家账号',
	`t_errPktTotal` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '出错包的总个数',
	`t_errPktRecent` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '当前出错包的个数',
	`t_kickoutTotal` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '被踢下线的总次数',
	`t_lastRstErrPktTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '上一次重置出错包的时间',
	`t_banLastTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '禁止登陆的截止时间',
	PRIMARY KEY (`t_accountId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_task`;
CREATE TABLE `t_task` (
	`t_accountId` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '玩家ID',
	`t_taskId` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '任务id',
	`t_taskContent` blob NOT NULL  COMMENT '任务内容',
	PRIMARY KEY (`t_accountId` , `t_taskId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_share4gold`;
CREATE TABLE `t_share4gold` (
	`t_owner` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '推广人ID',
	`t_createTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '记录ID',
	`t_targetAccount` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '被推广人ID',
	`t_nickName` varchar(50) NOT NULL DEFAULT '' COMMENT '被推广人昵称',
	`t_registerTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '被推广人注册时间',
	`t_gold` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '贡献金币',
	PRIMARY KEY (`t_createTime` , `t_targetAccount`),
	KEY `t_owner` (`t_owner`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_thirdPartyAcc`;
CREATE TABLE `t_thirdPartyAcc` (
	`t_accountID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '关联本地账户uid',
	`t_thirdPartyAcc` varchar(64) NOT NULL DEFAULT '' COMMENT '第三方账号',
	`t_channelKey` varchar(50) NOT NULL DEFAULT '' COMMENT '渠道key',
	`t_type` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '字段值 对应platform_sdk表 type字段 标识账号大类, 预留, 区分sdk',
	`t_extInfo` varchar(1024) NOT NULL DEFAULT '' COMMENT '第三方拓展信息',
	PRIMARY KEY (`t_accountID`),
	KEY `t_thirdPartyAcc` (`t_thirdPartyAcc`),KEY `t_channelKey` (`t_channelKey`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_weiXin`;
CREATE TABLE `t_weiXin` (
	`t_unionId` varchar(64) NOT NULL DEFAULT '' COMMENT '微信unionid',
	`t_channelKey` varchar(50) NOT NULL DEFAULT '' COMMENT '渠道key',
	`t_accountID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '关联本地账户uid',
	PRIMARY KEY (`t_unionId` , `t_channelKey`),
	KEY `t_accountID` (`t_accountID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_whiteList`;
CREATE TABLE `t_whiteList` (
	`t_accountName` varchar(30) NOT NULL DEFAULT '' ,
	`t_state` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '0--已删除 1--正常状态',
	PRIMARY KEY (`t_accountName`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_serverInfo`;
CREATE TABLE `t_serverInfo` (
	`t_id` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '主键1   此表是全局变量表  只会有一行记录  ',
	`t_srvState` int UNSIGNED NOT NULL DEFAULT 2 COMMENT '服务状态  1-维护 2-正常',
	`t_srvMaintStart` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '维护开始时间',
	`t_srvMaintEnd` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '维护结束时间',
	`t_accNameIdx` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '账号名idx',
	`t_nickIdx` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '昵称idx',
	PRIMARY KEY (`t_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_maintenanceList`;
CREATE TABLE `t_maintenanceList` (
	`t_gameId` int UNSIGNED NOT NULL DEFAULT 0 ,
	`t_state` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '1--维护状态 2--正常状态',
	`t_start` int UNSIGNED NOT NULL DEFAULT 0 ,
	`t_finish` int UNSIGNED NOT NULL DEFAULT 0 ,
	PRIMARY KEY (`t_gameId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_ai`;
CREATE TABLE `t_ai` (
	`t_id` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '机器人账号id',
	`t_nick` varchar(50) NOT NULL DEFAULT '' COMMENT '机器人昵称',
	`t_avatarID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '机器人头像',
	PRIMARY KEY (`t_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_offline`;
CREATE TABLE `t_offline` (
	`t_id` varchar(32) NOT NULL  COMMENT '记录Id',
	`t_accountID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '玩家id',
	`t_funcName` varchar(50) NOT NULL DEFAULT '' COMMENT '功能名称',
	`t_data` blob NOT NULL  COMMENT '处理数据',
	PRIMARY KEY (`t_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_rebateBalHist`;
CREATE TABLE `t_rebateBalHist` (
	`t_id` varchar(48) NOT NULL  COMMENT '记录Id',
	`t_accountID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '玩家id',
	`t_time` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '记录生成时间',
	`t_rebateCoin` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '提现金币',
	PRIMARY KEY (`t_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_exchangeCoinOutHist`;
CREATE TABLE `t_exchangeCoinOutHist` (
	`t_order` varchar(64) NOT NULL  COMMENT '订单ID',
	`t_accountID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '玩家id',
	`t_type` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '兑换方式 1-支付宝 2-银行卡',
	`t_exchangeCoin` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '兑换金币',
	`t_money` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '兑换金额',
	`t_status` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '状态',
	`t_time` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
	PRIMARY KEY (`t_order`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_purchaseList`;
CREATE TABLE `t_purchaseList` (
	`t_accountID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '玩家id',
	`t_itemID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '道具id',
	`t_count` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '道具数量',
	PRIMARY KEY (`t_accountID` , `t_itemID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_relation`;
CREATE TABLE `t_relation` (
	`t_accountID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '玩家id',
	`t_promoterID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '推广人ID',
	`t_extend` blob NOT NULL  COMMENT '推广扩展信息',
	PRIMARY KEY (`t_accountID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_rank`;
CREATE TABLE `t_rank` (
	`t_accountID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '玩家id',
	`t_nickName` varchar(30) NOT NULL DEFAULT '' COMMENT '昵称',
	`t_avaterId` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '头像',
	`t_frameId` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '头像框',
	`t_vipLevel` int UNSIGNED NOT NULL DEFAULT 0 COMMENT 'vip等级',
	`t_todayProfit` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '今日盈利',
	`t_todayProfitUpdateTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '今日盈利刷新时间',
	`t_todayOnlineTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '今日在线时间',
	`t_todayOnlineTimeupdateTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '今日在线时间刷新时间',
	PRIMARY KEY (`t_accountID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

