#/usr/bin/env python

#by Jack

import sys
import get_process
import games_resart_functions
import get_games_list
import time
import os
import send_msg
import logs
from run_linux_command import run_command

log_dir = '/tmp/'
games_list_dir = '/tmp/'
today_date = time.strftime('%Y-%m-%d', time.localtime(time.time()))
ops = ['zhangzhifeng']

# send_list ={'mj' :['zhangsan','lisi'],'buyu' :['zhangsan','lisi'],
#             'texas' :['zhangsan','lisi'],'ddz' :['zhangsan','lisi'],
#             'ox' :['zhangsan','lisi'],'pt' :['zhangsan','lisi','wangwu'],
#             'ops' : ['zhangsan','lisi'],'lucky25' : ['zhangsan','lisi'],
#             'dantiao': ['zhangsan', 'lisi'],'qqhar': ['zhangsan', 'lisi'],
#             'shuihu': ['zhangsan', 'lisi']}
#
# games_list_keys = {'mj': ['/app/game/joys/mj/cfg/', 'start_monitor.sh'],
#                    'texas': ['/app/game/joys/texas/cfg/', 'start_monitor.sh'],
#                    'buyu': ['/app/game/joys/buyu/cfg/', 'start_monitor.sh'],
#                    'ddz_jd': ['/app/game/joys/ddz/cfg/', 'start_monitor_jd.sh'],
#                    'ddz_hl': ['/app/game/joys/ddz/cfg/', 'start_monitor_hl.sh'],
#                    'ddz_hd': ['/app/game/joys/ddz/cfg/', 'start_monitor_hd.sh'],
#                    'ddz_er': ['/app/game/joys/ddz/cfg/', 'start_monitor_er.sh'],
#                    'ox': ['/app/game/joys/ox/cfg/', 'start_monitor.sh'],
#                    'lucky25': ['/app/game/joys/lucky25/cfg/', 'start_monitor.sh'],
#                    'dantiao': ['/app/game/joys/dantiao/cfg/', 'start_monitor.sh'],
#                    'qqhar': ['/app/game/joys/qqhar/cfg/', 'start_monitor.sh'],
#                    'shuihu': ['/app/game/joys/shuihu/cfg/', 'start_monitor.sh'],
#                    'pt_10.8.17.209': ['/app/game/platform/cfg/', 'monitor_start_10.8.17.209.sh'],
#                    'pt_10.8.17.210': ['/app/game/platform/cfg/', 'monitor_start_10.8.17.210.sh'],
#                    'pt_10.8.27.61': ['/app/game/platform/cfg/', 'monitor_start_10.8.27.61.sh'],
#                    'pt_10.8.27.63': ['/app/game/platform/cfg/', 'monitor_start_10.8.27.63.sh']}


def check_game_list_file():
    try:
        if os.path.getsize('{}game_check_error_svr.txt'.format(log_dir)):
            run_command('rm -f {}game_check_error_svr.txt'.format(log_dir),write_logs)
            write_logs.info('rm  {}game_check_error_svr.txt'.format(log_dir))
    except:
        write_logs.info('no such {}game_check_error_svr.txt,rm cancel'.format(log_dir))

    try:
        if os.path.exists(games_list_dir) and os.path.isfile('{}games_name.txt'.format(games_list_dir)) and os.path.getsize('{}games_name.txt'.format(games_list_dir)) is not 0:
            write_logs.info('games_list file is exists')


        else:
            write_logs.error('No such {}games_name.txt,script exitting...'.format(log_dir))

            now_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))
            error_msg = ('{} get {} list fail...,plase check monitoring file {}{}'.format(now_time, game_name,mnt_dir,mnt_file))
            send_msg.send_wechat(ops, error_msg,write_logs)

            exit()
    except:
        write_logs.error('script fail...')
        exit()

    game_srv_check(game_name)


def game_srv_check(game_name):

    g_f = open('{}games_name.txt'.format(games_list_dir),mode='r')

    for game_srv in g_f:
        running_status = get_process.get_process(game_srv.strip(),write_logs)

        if running_status == 1:
            write_logs.info(' {} is running... '.format(game_srv.strip()))
            # print('{} is running'.format(game_srv.strip()))

        else:
            write_logs.warning(' {} is not available! '.format(game_srv.strip()))

            with open('{}game_check_error_svr.txt'.format(log_dir),mode='a')as error_svr:
                error_svr.writelines(game_srv)

            now_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))
            error_msg = ('{} {} {} is not available! '.format(now_time, game_name, game_srv.strip()))
            if str(game_name).startswith('pt'):
                game_name_pt = 'pt'
                send_msg.send_wechat(send_list, error_msg,write_logs)

            elif str(game_name).startswith('ddz'):
                game_name_ddz = 'ddz'
                send_msg.send_wechat(send_list, error_msg,write_logs)

            else:
                send_msg.send_wechat(send_list, error_msg,write_logs)

    g_f.close()

    if os.path.isfile('{}game_check_error_svr.txt'.format(games_list_dir)) and os.path.getsize('{}game_check_error_svr.txt'.format(games_list_dir)):
            restart_game(game_name)

    else:
        write_logs.info('All service is ok...')

def restart_game(game_name):
    time.sleep(15)
    restart_svr = open('{}game_check_error_svr.txt'.format(log_dir),mode='r')
    for svr in restart_svr:

        if svr == '':
            break

        write_logs.warning('restartting {} '.format(svr.strip()))

        games_resart_functions.restartting(svr,write_logs,game_name,mnt_dir,mnt_file)

        time.sleep(1)

    run_command('echo > {0}game_check_error_svr.txt && sed -i  \'/^ *$/d\' {0}game_check_error_svr.txt'.format(log_dir),write_logs)
    restart_svr.close()

    run_command('rm {}games_check.lock'.format(games_list_dir),write_logs)


if __name__ == "__main__":
    arg_count = len(sys.argv)

    if  arg_count<5:
        print('Please import arguments  send_user|game_name|mnt_file_dir|mnt_file_name')
        # print(sys.argv[0] + r' game_name|send_user|mnt_file_dir|mnt_file')
        print('Usage:'+ sys.argv[0] + r' zhangzhifeng mj /app/game/joys/mj/cfg/ start_monitor.sh')
        exit()

    elif arg_count == 5:
        send_list = sys.argv[1].split(',')
        game_name = sys.argv[2]
        mnt_dir = sys.argv[3]
        mnt_file = sys.argv[4]
        write_logs = logs.write_logs(game_name)
        write_logs.info('rm {}games_name.txt'.format(log_dir))
        run_command('rm {}games_name.txt'.format(log_dir), write_logs)

        get_games_list.get_list(write_logs, game_name, mnt_dir, mnt_file)

#for disc
        # if game_name in games_list_keys.keys() and os.path.isfile('{}{}'.format(mnt_dir,mnt_file)):
        #     get_games_list.get_list(write_logs,game_name,mnt_dir,mnt_file)

        # else:
        #     write_logs.error('Unknow game name or monitor file no such...')

    check_game_list_file()
