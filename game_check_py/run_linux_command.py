#/usr/bin/env python

# -*- coding: utf-8 -*-

#by Jack

import subprocess


def run_command(cmd,write_logs=None,stdout=None,):
    if stdout:
        subprocess.Popen(cmd, shell=True,stderr=subprocess.STDOUT)

    else:
        s = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        stdout, stderr = s.communicate()
        return_code = s.returncode
        if return_code != 0 and write_logs is not None:
            write_logs.error(stderr)
        return return_code,stdout,stderr

