<?php
/**
 * $splitChar 字段分隔符
 * $file 数据文件文件名
 * $table 数据库表名
 * $conn 数据库连接
 * $fields 数据对应的列名
 * $insertType 插入操作类型，包括INSERT,REPLACE
 */
function loadTxtDataIntoDatabase($pid_file,$splitChar,$file,$table,$conn,$fields=array(),$insertType='LOAD'){
  	//归档

	$table = archiver_day($pid_file,$conn,$table,$file);
 	$file=check_err_format($file,count($fields)-1,$conn,$splitChar,$table,$fields);
	if(!$file){
		exit(233);

	}
	if($insertType=='LOAD'){
		if(!empty($fields)){
			$query="load data local infile '{$file}' into table {$table}  fields terminated by '${splitChar}'  lines terminated by '\\n' (" .implode(',',$fields).");";
			//echo $query.PHP_EOL;
		}
		else{
			$query="load data local infile '{$file}' into table {$table} fields terminated by '${splitChar}'  lines terminated by '\\n';";
		//echo $query.PHP_EOL;
		}

	}
	else{
		$result = insertMethod($splitChar,file,$table,$conn,$fields,$type='file');


	}
	if(!isset($conn) ||!is_resource($conn) || !mysql_ping($conn)){
		$ret_con = false;
		for($j=0;$j<5;$j++){
			$conn=mysql_connect("${db_config['db_hostname']}:${db_config['db_port']}","${db_config['db_user']}","${db_config['db_password']}");
			mysql_select_db("${db_config['db_name']}",$conn);
			if(isset($conn) && is_resource($conn) && mysql_ping($conn)){
				$ret_con = true;
				break;
				
			}
			sleep(1);
			
		}
        
        if(!$ret_con){

            echo "loadTxtDataIntoDatabase重连失败".PHP_EOL;
            exit;

        }
	}
    
	$res=mysql_query($query,$conn);
	
	if(!$res){
		if(isset($conn) && is_resource($conn) && mysql_ping($conn)){
			insertTwice($splitChar,$file,$table,$fields,$conn);
		}else{
			mysql_close($conn);
			echo "连接已经断开".PHP_EOL;
			$conn=mysql_connect("${db_config['db_hostname']}:${db_config['db_port']}","${db_config['db_user']}","${db_config['db_password']}");
			mysql_select_db("{$db_config['db_name']}",$conn);
			insertTwice($splitChar,$file,$table,$fields,$conn);

		}


	}

	if(substr(basename($file),0,4)=='tmp_'&&file_exists($file)){
		echo "开始清理临时表:".$file.PHP_EOL;
       		for($i=0;$i<=6;$i++){
                unlink($file);
              	if(!file_exists($file)){
                    break;

              	}
        	}


	}

	return array(true);
}


/*****************
/* $arch_num归档阈值，超过这个数据，则归档
*********************/

function archiver_day($pid_file,$conn,$table,$file){
	require 'table_split.php';
	if(!isset($conn) ||!is_resource($conn) || !mysql_ping($conn)){
		$ret_con = false;
		for($j=0;$j<5;$j++){
			$conn=mysql_connect("${db_config['db_hostname']}:${db_config['db_port']}","${db_config['db_user']}","${db_config['db_password']}");
			mysql_select_db("${db_config['db_name']}",$conn);
			if(isset($conn) && is_resource($conn) && mysql_ping($conn)){
				$ret_con = true;
				break;
				
			}
			sleep(1);
			
		}
        
        if(!$ret_con){

            echo "archiver_day重连失败".PHP_EOL;
            exit;

        }
	}
	$f_time = (int)substr($file,-14);
	$f_date = substr($f_time,0,8);
    $sql="show tables like '".${table}."2%'";
    $result=mysql_query($sql,$conn);
    
    if(!$result){
        echo "分表检查连接数据库异常".PHP_EOL;
        file_put_contents($pid_file,"finish");
        exit;

	}
    while($row=mysql_fetch_array($result)){
        $d_times[] = (int)substr($row[0],-14);


    }

    $is_ymd=13;
    $beginDate = date('Y-m-01',strtotime($f_time));
    $m_sumeter_date = (string)date("Ymd",strtotime("$beginDate +1 month -1 day"));
    $year=date("Y",strtotime($f_time));
    $y_sumeter_date = $year."1231";
    

    if(isset($d_times) && !empty($d_times)){
		rsort($d_times);

        foreach($d_times as $d_time){
	    $d_date =  substr($d_time,0,8);
	    /*按年月天分表*/
            if($f_time < $d_time && $f_date==$d_date && in_array($table,$day_sumeter) || in_array($table,$mon_sumeter) && $m_sumeter_date==$d_date||
	    ! in_array($table,$day_sumeter) && ! in_array($table,$mon_sumeter) && $y_sumeter_date == $d_date){
                $table = $table.$d_time;
                return $table;


            }



	

        }
	
    }
    if(in_array($table,$day_sumeter)){
	$is_ymd=11;


    }
   if(in_array($table,$mon_sumeter)){
	$is_ymd=12;


    }

    $table = archiver($is_ymd,$pid_file,$conn,$table,$file);


    return $table;

}


function archiver($is_ymd,$pid_file,$conn,$table,$file){
	if(!isset($conn) ||!is_resource($conn) || !mysql_ping($conn)){
		$ret_con = false;
		for($j=0;$j<5;$j++){
			$conn=mysql_connect("${db_config['db_hostname']}:${db_config['db_port']}","${db_config['db_user']}","${db_config['db_password']}");
			mysql_select_db("${db_config['db_name']}",$conn);
			if(isset($conn) && is_resource($conn) && mysql_ping($conn)){
				$ret_con = true;
				break;
				
			}
			sleep(1);
			
		}
        
        if(!$ret_con){

            echo "archiver重连失败".PHP_EOL;
            exit;

        }
	}
        //rename table
    $table_time=substr($file,-14);
    echo $table_time.$file;
    if($is_ymd==11){


         $table_suffix = substr($table_time,0,8)."235959";
    }elseif($is_ymd==12){
	$beginDate = date('Y-m-01',strtotime($table_time));
	$m_sumeter_date = (string)date("Ymd",strtotime("$beginDate +1 month -1 day"));
	$table_suffix = $m_sumeter_date."235959";
    }else{
    	$year=date("Y",strtotime($table_time));
        $y_sumeter_date = $year."1231";
	$table_suffix = $y_sumeter_date."235959";



    }
    echo "table_suffix is ".$table_suffix.PHP_EOL;
    $get_time=strtotime($table_suffix);
    $rename_name=$table.$table_suffix;


    $sql="rename table ${table} to ${rename_name}";

    if(!mysql_query($sql,$conn)){
        $content="rename table $table failed <br \>";
        //sendMail($content);
        //sendWechat($content);
        echo $content.PHP_EOL;
        file_put_contents($pid_file,"finish");
        exit(211);

    }

        //create new table
    $sql="create table ${table} like $rename_name;";
    if(!mysql_query($sql,$conn)){
        $content="create table $table failed <br \>";
        //sendMail($content);
        //sendWechat($content);
        echo $content.PHP_EOL;
        exit(212);


    }

    //create router table && insert data
    $router="RouterTableLog";
    $sql="create table if not exists  ${router} (id int primary key auto_increment,table_prefix varchar(50) not null,table_name varchar(50) not null,gen_time int UNSIGNED not null,is_ymd int NOT NULL)";
    if(!mysql_query($sql,$conn)){

        echo "create router table failed!".PHP_EOL;
        exit(213);


    }
    $sql="insert into $router (table_prefix,gen_time,table_name,is_ymd) values('".$table."',".$get_time.",'".$rename_name."','".$is_ymd."')";
    if(!mysql_query($sql,$conn)){

        echo "router table insert failed".$sql.PHP_EOL;
        exit(214);
    }

	return $rename_name;


}
//检查日志入库的文件是否格式存在错误
function check_err_format($file,$count,$conn,$splitChar,$table,$fields){
	if(!isset($conn) ||!is_reso/($conn) || !mysql_ping($conn)){
		$ret_con = false;
		for($j=0;$j<5;$j++){
			$conn=mysql_connect("${db_config['db_hostname']}:${db_config['db_port']}","${db_config['db_user']}","${db_config['db_password']}");
			mysql_select_db("${db_config['db_name']}",$conn);
			if(isset($conn) && is_resource($conn) && mysql_ping($conn)){
				$ret_con = true;
				break;
				
			}
			sleep(1);
			
		}
        
        if(!$ret_con){

            echo "check_err_format重连失败".PHP_EOL;
            exit;

        }
	}
    $line_err=0;
    $line_number=0;
    if(!file_exists($file)){
        echo "check_err_format:$file not exists \n";
        return false;

    }
    if( filesize($file) <=1 ){
        echo $file."空文件无需判断内容".PHP_EOL;
        return $file;

    }
    $file_array = file($file);
    foreach($file_array as $line){
        $line_number++;
        if(substr_count($line,'|')!=$count){

            $line_err++;
            $index=$line_number-1;
            unset($file_array["${index}"]);
            $content=$file.'第'.$line_number."行数据有误,缺少字段<br \>";
            //sendMail($content);
            //sendWechat($content);
            echo $content.PHP_EOL;
            continue;


        }
        if(!mb_detect_encoding($line, 'UTF-8', true)){

            $line_err++;
            $index=$line_number-1;
            unset($file_array["${index}"]);
            $content=$file.'第'.$line_number."编码有误，非UTF-8字符!<br \>";
            //sendMail($content);
            //sendWechat($content);
            echo $content.PHP_EOL;
            continue;


        }

        if(testEmoji($line)){
            $line_err++;
            $index=$line_number-1;
            unset($file_array["${index}"]);
            $sql = insertMethod($splitChar,$line,$table,$fields);
            //echo $sql;
            mysql_query("set names utf8mb4",$conn);
            $res = mysql_query($sql,$conn);
            if(!$res){
                $content = "第".$line_number."行utf8mb4字符入库错误，错误号为:".mysql_errno($conn)."错误为".mysql_error($conn).'\n<br \>';
                //sendMail($content);
                //sendWechat($content);
                echo $content.PHP_EOL;

            }


        }


    }




    if($line_err !=0){
		$file=dirname($file)."/tmp_".basename($file);

		file_put_contents($file,$file_array);


    }


	return $file;
}



function testEmoji($str){
	$text = json_encode($str); //暴露出unicode
	return preg_match("/(\\\u[ed][0-9a-f]{3})/i",$text);
}

function insertTwice($splitChar,$file,$table,$fields,$conn){
	if(!isset($conn) ||!is_resource($conn) || !mysql_ping($conn)){
		$ret_con = false;
		for($j=0;$j<5;$j++){
			$conn=mysql_connect("${db_config['db_hostname']}:${db_config['db_port']}","${db_config['db_user']}","${db_config['db_password']}");
			mysql_select_db("${db_config['db_name']}",$conn);
			if(isset($conn) && is_resource($conn) && mysql_ping($conn)){
				$ret_con = true;
				break;
				
			}
			sleep(1);
			
		}
        
        if(!$ret_con){

            echo "insertTwice重连失败".PHP_EOL;
            exit;

        }
	}
	$file_array = file($file);
	$line_number=0;
	 foreach($file_array as $line){
		$line_number++;
		$sql = insertMethod($splitChar,$line,$table,$fields);
	//	echo $sql.PHP_EOL;
		$res = mysql_query($sql,$conn);
		if(!$res){
			$content = $file."第".$line_number."行字符入库错误，错误号为:".mysql_errno($conn)."错误为".mysql_error($conn).'\n<br \>';
			//sendMail($content);
			//sendWechat($content);
                        echo $content.PHP_EOL;


		}

	}


}

function insertMethod($splitChar,$file,$table,$fields,$type='line'){
	if(empty($fields)) $head = "INSERT INTO `{$table}` VALUES('";
	else $head = "INSERT INTO `{$table}`(`".implode('`,`',$fields)."`) VALUES('";  //数据头
	$end = "')";
	if($type=='file'){
		$sqldatas = trim(file_get_contents($file));
	}else{
		$sqldatas= trim($file);

	}
	$sqldata = mysql_real_escape_string($sqldatas);
	if(empty($sqldata)){
		return array(true);
	}
	if(preg_replace('/\s*/i','',$splitChar) == '') {
		$splitChar = '/(\w+)(\s+)/i';
		$replace = "$1','";
		$specialFunc = 'preg_replace';
	}else {
		$splitChar = $splitChar;
		$replace = "','";
		$specialFunc = 'str_replace';
	}
	//处理数据体，二者顺序不可换，否则空格或Tab分隔符时出错
	$sqldata = preg_replace('/(\s*)(\n+)(\s*)/i','\'),(\'',$sqldata);  //替换换行
	$sqldata = $specialFunc($splitChar,$replace,$sqldata);        //替换分隔符
	$query = $head.$sqldata.$end;  //数据拼接
	return $query;
}
