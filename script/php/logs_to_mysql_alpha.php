<?php
error_reporting(E_ALL & ~E_NOTICE);
require 'db_config.php';
require 'import_new.php';
require 'need_file_new.php';
#require 'sendmail_use_curl.php';
//array_shift($agrv);
date_default_timezone_set('Etc/GMT-8');
$dirname="$argv[1]";
$splitChar = '|';  //竖线
$conn=mysql_connect("${db_config['db_hostname']}:${db_config['db_port']}","${db_config['db_user']}","${db_config['db_password']}");
echo $db_config['db_name'];
mysql_select_db("${db_config['db_name']}",$conn);
$position_dir="$argv[2]";
$position_file="$argv[3]";

$key_part = "$argv[4]";
//允许20分钟延迟
$allow_time=900;
//获取当前时间
$cur_system_time = time();
if(empty($cur_system_time)){
        echo "failed:获取时间戳失败！！".PHP_EOL;
        exit;


}
//$cr_value = 2;

//当前脚本在执行，则退出脚本
//if(check_has_run($key_part,$cr_value)){
//        echo "failed:当前脚本已经在执行".PHP_EOL;
//        exit;
//}

//求得PID文件

$division = substr(rtrim($position_dir,'/'),-2);
if(!is_numeric($division)){
	$division = substr(rtrim($position_dir,'/'),-1);


}


$pid_file=rtrim($position_dir,'/').'/'.$key_part;
$log_file=dirname(rtrim($position_dir,'/')).'/log/load_log'.$division.'.log';
$pos_file=rtrim($position_dir,'/').'/'.$position_file;
if(check_proc_run($dirname,3)){

        exit;

}


if(!file_exists($pos_file)){
	
	if(!InitPid($pid_file)){
		echo "初始化进程ID文件失败".PHP_EOL;

	}


}

//进程上锁
$handler = fopen($pid_file,"r");
if(!$handler){
        echo "进程预上锁失败，程序退出".PHP_EOL;

}
flock($handler,LOCK_EX);
if(!InitPosition($pos_file)){

	exit;

}



if(!position_recovery($pid_file,$pos_file,$log_file,$dirname)){
        exit;

}


//获取已经导入数据文件的位置，如果没配置文件，则自动创建
$get_position = getPreviousTimes($pos_file);
if(!isset($get_position) || !$get_position){
	echo "获取位置存在问题".PHP_EOL;
	exit;

}
//print_r($get_position);
if($get_position){
	if($get_position == "none"){

		$positions = array();
	}elseif(stristr($get_position,"array")){
		$str_array_position = '$positions='.$get_position.';';
		eval("$str_array_position");
	}else{

		echo "不规范的位置结构".PHP_EOL;
		exit;
	}

}



$table_pattens='/^[a-zA-Z]*/';
$number_pattens='/[1-9][0-9]*$/';





//获取一个数组，是表名与时间的一个对应关系
//对于一个日志文件，最新的文件可能在写入，所以不能导入
$table_to_times = getTableTimeArray($table_pattens,$number_pattens,$dirname);
//print_r($table_to_times);
//exit;

//获取最新日志文件的时间值，用于比较，拥有新时间值得不会导入，因为该文件可能在写入
$max_names = getMaxTimeFileNames($table_to_times,rtrim($position_dir,'/').'/'.$position_file);
//exit;



foreach($max_names as $table => $number){
    //当前时间-最后一个文件时间，如果时间大于20分钟，说明文件已经不在写，可以入库 
    $last_file_time = strtotime($number);
    $intervals = $cur_system_time-$last_file_time;
    if(empty($last_file_time)&&!is_numeric($last_file_time)||$intervals<0){

                $intervals=$allow_time-1;

    }
    foreach($table_to_times as $key => $values){
	sort($values);

        if($table == $key){
	    //判断数据是否有变化
	    $change=0;
            for($i=0;$i<count($values);$i++){
		
		//如果一个文件的位置在位置文件中以及存在，则用前者来导入数据，否则用后者
                if((!empty($positions["$table"]) && $values["$i"] >= $positions["$table"] || empty($positions["$table"]) )  && ($values["$i"] < $number|| ($values["$i"]==$number&&$intervals>$allow_time) ) ){
		    
                    $file=$table.$values["$i"];
		    //空文件会出现load出错
		    $fields=getFieldsFromTable($table,$conn);
		    if($fields){
		        $result = loadTxtDataIntoDatabase($pid_file,$splitChar,rtrim($dirname,'/').'/'.$file,$table,$conn,$fields);
		    }
		     
		    if (!empty($result)&&array_shift($result)){
                        echo 'load|'.$file.'|Success!'."导入时间是:".date('y-m-d h:i:s',time()).PHP_EOL;
			//如果有load数据，说明位置有变化
			if(!$change){
			    $change=1;

			}
			if($values["$i"]==$number&&$intervals>$allow_time){
				$max_names["$table"]=$values["$i"]+1;
			}
		    }else {
			$max_names["$table"]=$values["$i"];
			$content="load|$file|failed!<br \>"."导入时间是:".date('y-m-d h:i:s',time()).PHP_EOL;
			//sendMail($content);
            		//sendWechat($content);
                        echo $content.PHP_EOL;
			break;
                    }
		    

                }
		/*else{
		    if(!empty($positions["$table"])){

			$max_names["$table"] = $positions["$table"];
			echo "未变化的位置表是：".$max_names["$table"].PHP_EOL;
		    }
		}*/
		
		
            }
	    if( !empty($positions["$table"]) && $change==0 ){
		$max_names["$table"] = $positions["$table"];
		//echo "未变化的位置表是：".$max_names["$table"].PHP_EOL;

	    }
	}
    }

}
$n_times=1;
while($n_times>0){
	if(!file_put_contents(rtrim($position_dir,'/').'/'.$position_file,var_export($max_names, TRUE))){
		sleep(5);
		echo "put position waitting...".PHP_EOL;
		continue;

	}
	break;

}

//解除进程锁
file_put_contents($pid_file,"finish");
flock($handler,LOCK_UN);
