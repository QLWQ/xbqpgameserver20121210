#!/usr/local/php/bin/php -q
<?php


function getDirList($dir){
	$dirs=array();
	if(!is_dir($dir)){
		return false;
	}
	
	$dir_list=scandir($dir);
	foreach($dir_list as $dirfile){
		if(is_dir($dirfile)){
			$dirs[] = $dir.'/'.$dirfile;
		}
	}

	return $dirs;
}
