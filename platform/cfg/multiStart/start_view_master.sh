#!/bin/bash

ssh login nohup tail -f /app/game/platform/log/log/master/* >> master.log 2>&1 & 
ssh web nohup tail -f /app/game/platform/log/log/master/* >> master.log 2>&1 & 
ssh dbp nohup tail -f /app/game/platform/log/log/master/* >> master.log 2>&1 &
