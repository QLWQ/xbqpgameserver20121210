#!/bin/bash

runpath=$PWD

PIDS=`ps -ef | grep influxd | grep -v grep | grep -v 10000 | awk '{print $2}'`
for PID in $PIDS
do
	kill $PID
done

