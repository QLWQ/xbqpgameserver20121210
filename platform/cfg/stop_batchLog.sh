#!/bin/bash

runpath=$PWD

PIDS=`ps -ef | grep -v grep | grep -v 10000 | grep .xml | grep batchLog | grep $runpath | awk '{print $2}'`
for PID in $PIDS
do
	kill $PID
done

