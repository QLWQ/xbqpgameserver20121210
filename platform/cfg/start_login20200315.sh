#!/bin/bash

ulimit -c unlimited

ulimit -n 60000
dbs_path=$PWD/db/virdbs.xml


# flag=`/usr/sbin/ifconfig|grep "192.168.1.130"`
# if [ "$flag" == "" ]; then
	# ret=`grep "game_run" $dbs_path -Rn`
	# if [ "$ret" != "" ]; then
		# echo "非内测服务器禁止连接game_run数据库，请修改db/dbs.xml"
	# exit
	# fi
# fi

curpath=$PWD
xml_path=$PWD/common.xml

cd ../bin

if [ ! -d ./log/log/ ]; then
	mkdir -p ./log/log/
	chmod 777 ./log/log/
fi

if [ "$1" == "jekins" ]; then
	VALGRIND="valgrind --tool=memcheck --leak-check=full --track-origins=yes --show-reachable=yes
        --show-leak-kinds=all --xml=yes --xml-file=valgrind"
elif [ "$1" == "v" ]; then
	VALGRIND="valgrind --tool=memcheck --leak-check=full --track-origins=yes --show-reachable=yes
	--show-leak-kinds=all --log-file=valgrind"
fi



	./login $xml_path "login1" "$curpath" & > /dev/null 2>&1
	sleep 10
	./login $xml_path "login2" "$curpath" & > /dev/null 2>&1
