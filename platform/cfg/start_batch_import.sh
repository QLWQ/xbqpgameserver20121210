#!/bin/bash

ulimit -c unlimited
ulimit -n 60000
curpath=$PWD

cd ../bin

if [ ! -d ./log/log/ ]; then
	mkdir -p ./log/log/
	chmod 777 ./log/log/
fi

if [ "$1" == "jekins" ] || [ "$1" == "v" ]; then
	if [ ! -d "valgrind" ]; then
	mkdir valgrind
	fi
else
	./login $xml_path "login3" "1003" "$curpath" & > /dev/null 2>&1
	sleep 5
fi
