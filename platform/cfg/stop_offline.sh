#!/bin/bash

runpath=$PWD

#stop_state.sh

PIDS=`ps -ef | grep -v grep | grep -v 10000 | grep .xml | grep offline | grep $runpath | awk '{print $2}'`
for PID in $PIDS
do
	kill $PID
done

