#!/bin/bash
#本脚本只是针对单个进程启动使用，适用场景是运维的监控系统自动调用
chmod 777 *

cd /app/wjh/influxdb/usr/bin
nohup ./influxd -config ../../etc/influxdb/influxdb.conf &
sleep 1