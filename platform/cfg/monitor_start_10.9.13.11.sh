#!/bin/bash
#本脚本只是针对单个机器的进程启动使用，适用场景是运维的监控系统自动调用
#田斌说不需要写master的

./monitor_start_dbproxy.sh dbproxy1
./monitor_start_state.sh state1
./monitor_start_AI.sh AI1
./monitor_start_bulletin.sh bulletin
./monitor_start_list.sh list
./monitor_start_mail.sh mail
./monitor_start_name.sh name
./monitor_start_relation.sh relation
./monitor_start_brief.sh brief1
./monitor_start_collector.sh collector1
./monitor_start_offline.sh offline1
./monitor_start_rank.sh rank1
./monitor_start_history.sh history1
./monitor_start_logger.sh logger1
./monitor_start_hall.sh hall1
./monitor_start_login.sh login1

