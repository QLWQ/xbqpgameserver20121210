#!/bin/bash
#本脚本只是针对单个进程启动使用，适用场景是运维的监控系统自动调用
chmod 777 *

ulimit -c unlimited

ulimit -n 60000
dbs_path=$PWD/db/virdbs.xml


curpath=$PWD
xml_path=$PWD/common.xml

cd ../bin
./login $xml_path "$1" "$curpath" & > /dev/null 2>&1
sleep 1
