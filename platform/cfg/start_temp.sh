#!/bin/bash

ulimit -c unlimited

ulimit -n 60000
dbs_path=$PWD/db/virdbs.xml


# flag=`/usr/sbin/ifconfig|grep "192.168.1.130"`
# if [ "$flag" == "" ]; then
	# ret=`grep "game_run" $dbs_path -Rn`
	# if [ "$ret" != "" ]; then
		# echo "非内测服务器禁止连接game_run数据库，请修改db/dbs.xml"
	# exit
	# fi
# fi

#启动时 关掉login服务
./stop_login.sh 
echo "stop_login done"
sleep 10

#启动时 关掉日志服务
./stop_batchLog.sh 
echo "stop_batchLog done"
sleep 10


./stop_dwh.sh 
echo "stop_dwh done"
sleep 10


#启动时 关掉master服务
./stop_master.sh 
echo "stop_master done"
sleep 30

curpath=$PWD
xml_path=$PWD/common.xml

cd ../bin

if [ ! -d ./log/log/ ]; then
	mkdir -p ./log/log/
	chmod 777 ./log/log/
fi

if [ "$1" == "jekins" ]; then
	VALGRIND="valgrind --tool=memcheck --leak-check=full --track-origins=yes --show-reachable=yes
        --show-leak-kinds=all --xml=yes --xml-file=valgrind"
elif [ "$1" == "v" ]; then
	VALGRIND="valgrind --tool=memcheck --leak-check=full --track-origins=yes --show-reachable=yes
	--show-leak-kinds=all --log-file=valgrind"
fi



if [ "$1" == "jekins" ] || [ "$1" == "v" ]; then
	if [ ! -d "valgrind" ]; then
	mkdir valgrind
	fi

	
	$VALGRIND/master-%p ./master $xml_path "master1" "$curpath" & > /dev/null 2>&1
	sleep 3
	$VALGRIND/dwh-%p ./dwh $xml_path "dwh1" "$curpath" & > /dev/null 2>&1
	sleep 3
	$VALGRIND/dbproxy-%p ./dbproxy $xml_path "dbproxy1" "$curpath" & > /dev/null 2>&1
	sleep 3
	$VALGRIND/name-%p ./name $xml_path "$curpath" & > /dev/null 2>&1
	sleep 3
	$VALGRIND/relation-%p ./relation $xml_path "$curpath" & > /dev/null 2>&1
	sleep 3
	$VALGRIND/web-%p ./web $xml_path "web1" "$curpath" & > /dev/null 2>&1
	sleep 3
	$VALGRIND/state-%p ./state $xml_path "state1" "$curpath" & > /dev/null 2>&1
	sleep 3
	$VALGRIND/mail-%p ./mail $xml_path  "$curpath" & > /dev/null 2>&1
	sleep 3
	$VALGRIND/bulletin-%p ./bulletin $xml_path "$curpath"  & > /dev/null 2>&1
	sleep 3
	$VALGRIND/list-%p ./list $xml_path "list" "$curpath" & > /dev/null 2>&1
	sleep 3
	$VALGRIND/hall-%p ./hall $xml_path "hall1" "$curpath" & > /dev/null 2>&1
	sleep 3
	$VALGRIND/login-%p ./login $xml_path "login1" "$curpath" & > /dev/null 2>&1
	sleep 3
	#$VALGRIND/brief-%p ./brief $xml_path "brief1" "$curpath" & > /dev/null 2>&1
	#sleep 3
	$VALGRIND/AI-%p ./AI $xml_path "AI1" "$curpath" & > /dev/null 2>&1
	sleep 3
	#$VALGRIND/collector-%p ./collector $xml_path "collector1" "$curpath" & > /dev/null 2>&1
	#sleep 3
	$VALGRIND/history-%p ./history $xml_path "history1" "$curpath" & > /dev/null 2>&1
	#sleep 3
	#$VALGRIND/offline-%p ./offline $xml_path "offline1" "$curpath" & > /dev/null 2>&1
else
	./master $xml_path "master1" "$curpath" & > /dev/null 2>&1
	sleep 10
	./master $xml_path "master2" "$curpath" & > /dev/null 2>&1
	sleep 10
	./master $xml_path "master3" "$curpath" & > /dev/null 2>&1
	sleep 10
	./master $xml_path "master4" "$curpath" & > /dev/null 2>&1
	sleep 10
	./batchLog $xml_path "logger10" "$curpath" & > /dev/null 2>&1
	sleep 30
	./batchLog $xml_path "logger3" "$curpath" & > /dev/null 2>&1
	sleep 30
	./batchLog $xml_path "logger2" "$curpath" & > /dev/null 2>&1
	sleep 30
	./batchLog $xml_path "logger1" "$curpath" & > /dev/null 2>&1

	sleep 30
	./dwh $xml_path "dwh1" "$curpath" & > /dev/null 2>&1
	sleep 10
	./dwh $xml_path "dwh2" "$curpath" & > /dev/null 2>&1
	sleep 10
	./dwh $xml_path "dwh3" "$curpath" & > /dev/null 2>&1
	sleep 10
	./dwh $xml_path "dwh4" "$curpath" & > /dev/null 2>&1
	sleep 10
	./dbproxy $xml_path "dbproxy1" "$curpath" & > /dev/null 2>&1
	sleep 10
	./name $xml_path "$curpath" & > /dev/null 2>&1
	sleep 10
	./relation $xml_path "1" "$curpath" & > /dev/null 2>&1
	sleep 10
	./web $xml_path "web1" "$curpath" & > /dev/null 2>&1
	sleep 10
	./state $xml_path "state1" "$curpath" & > /dev/null 2>&1
	sleep 10
	./mail $xml_path  "$curpath" & > /dev/null 2>&1
	sleep 10
	./bulletin $xml_path "$curpath"  & > /dev/null 2>&1
	sleep 10
	./list $xml_path "list" "$curpath" & > /dev/null 2>&1
	sleep 10
	./hall $xml_path "hall1" "$curpath" & > /dev/null 2>&1
	sleep 10
	./hall $xml_path "hall2" "$curpath" & > /dev/null 2>&1
	sleep 10
	./hall $xml_path "hall3" "$curpath" & > /dev/null 2>&1
	sleep 10
	./hall $xml_path "hall4" "$curpath" & > /dev/null 2>&1
	sleep 10
	./login $xml_path "login1" "$curpath" & > /dev/null 2>&1
	sleep 10
	./login $xml_path "login2" "$curpath" & > /dev/null 2>&1
	sleep 10
	#./brief $xml_path "brief1" "$curpath" & > /dev/null 2>&1
	#sleep 10
	./AI $xml_path "AI1" "$curpath" & > /dev/null 2>&1
	sleep 10
	#./collector $xml_path "collector1" "$curpath" & > /dev/null 2>&1
	#sleep 10
	./history $xml_path "history1" "$curpath" & > /dev/null 2>&1
	sleep 10
	#./offline $xml_path "offline1" "$curpath" & > /dev/null 2>&1
	#sleep 10
	./rank $xml_path "rank1" "$curpath" & > /dev/null 2>&1
fi
cd ../cfg
./start-cgi.sh
