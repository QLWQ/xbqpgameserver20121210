alter table t_userBaseAttr add column `t_lastGameBetScore` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '昨天玩家下注积分';
alter table t_userBaseAttr add column `t_todayGameBetScore` int UNSIGNED  DEFAULT 0 COMMENT '今天玩家下注积分';
alter table t_userBaseAttr add column `t_betScoreRsfTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '玩家下注金币刷新时间';

DROP TABLE IF EXISTS `t_bulletin2Channel`;
CREATE TABLE `t_bulletin2Channel` (
	`t_bulletinId` varchar(150) NOT NULL DEFAULT '' COMMENT '公告ID',
	`t_channelId` varchar(16) NOT NULL DEFAULT '' COMMENT '如果为空表示针对所有渠道',
	PRIMARY KEY (`t_bulletinId` , `t_channelId`),
	KEY `t_bulletinId` (`t_bulletinId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_recharge_order`;
CREATE TABLE `t_recharge_order` (
	`t_orderId` varchar(100) NOT NULL DEFAULT '' COMMENT '订单号',
	`t_accountId` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '玩家ID',
	`t_type` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '货币类型',
	`t_amount` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '货币数量',
	`t_createTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
	`t_status` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '当前状态',
	PRIMARY KEY (`t_orderId`),
	KEY `t_orderId` (`t_orderId`),KEY `t_accountId` (`t_accountId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;


ALTER TABLE t_task ADD INDEX task_acc_idx(t_accountId);
ALTER TABLE t_offline ADD INDEX offline_acc_idx(t_accountID);
ALTER TABLE t_rebateBalHist ADD INDEX rebate_acc_idx(t_accountID);
ALTER TABLE t_exchangeCoinOutHist ADD INDEX exchange_acc_idx(t_accountID);
ALTER TABLE t_offlineOrder ADD INDEX offlineOrder_acc_idx(t_accountID);


DROP TABLE IF EXISTS `t_luckyRoulette`;
CREATE TABLE `t_luckyRoulette` (
	`t_channel` varchar(64) NOT NULL DEFAULT '' COMMENT '渠道ID',
	`t_silverCfg` blob   COMMENT '白银转盘数据',
	`t_silverCost` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '白银转盘单次抽奖消耗金币',
	`t_goldCfg` blob   COMMENT '黄金转盘数据',
	`t_goldCost` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '黄金转盘单次抽奖消耗金币',
	`t_diamondCfg` blob   COMMENT '钻石转盘数据',
	`t_diamondCost` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '钻石转盘单次抽奖消耗金币',
	`t_start` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '活动开始时间',
	`t_finish` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '活动结束时间',
	PRIMARY KEY (`t_channel`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_rouletteHist`;
CREATE TABLE `t_rouletteHist` (
	`t_id` varchar(48) NOT NULL  COMMENT '记录Id',
	`t_accountID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '玩家id',
	`t_nickName` varchar(30) NOT NULL DEFAULT '' COMMENT '昵称',
	`t_time` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '记录生成时间',
	`t_type` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '夺宝类型 1-白银 2-黄金 3-钻石',
	`t_award` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '获奖金额',
	PRIMARY KEY (`t_id`),
	KEY `t_accountID` (`t_accountID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;