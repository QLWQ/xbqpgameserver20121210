DROP TABLE IF EXISTS `t_channelVip`;
CREATE TABLE `t_channelVip` (
	`t_channel` varchar(64) NOT NULL DEFAULT '' COMMENT '渠道ID',
	`t_cfgTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '配置时间',
	`t_openType` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '开关类型',
	`t_vipInfo` blob   COMMENT '渠道VIP信息',
	PRIMARY KEY (`t_channel`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `t_channelSuccour`;
CREATE TABLE `t_channelSuccour` (
	`t_channel` varchar(64) NOT NULL DEFAULT '' COMMENT '渠道ID',
	`t_cfgTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '配置时间',
	`t_openType` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '开关类型',
	`t_succourInfo` blob   COMMENT '渠道救助金信息',
	PRIMARY KEY (`t_channel`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

alter table `t_userBaseAttr` add column `t_vipInfo` blob   COMMENT 'vip信息';
alter table `t_userBaseAttr` drop column `t_vipLevel`;


alter table `t_userBaseAttr` add column `t_succourCnt` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '已领取救济金次数';
alter table `t_userBaseAttr` add column `t_lastReciveSuccourTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '上次领取救济金时间';

drop table t_task;

DROP TABLE IF EXISTS `t_channelTask`;
CREATE TABLE `t_channelTask` (
	`t_channel` varchar(64) NOT NULL DEFAULT '' COMMENT '渠道ID',
	`t_activityOpenType` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '活动开关类型',
	`t_activityTimeType` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '活动时间类型',
	`t_beginTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '活动开始时间',
	`t_endTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '活动结束时间',
	`t_cfgTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '配置时间',
	`t_taskInfo` blob   COMMENT '渠道任务信息',
	PRIMARY KEY (`t_channel`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_playerTask`;
CREATE TABLE `t_playerTask` (
	`t_accountId` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '玩家ID',
	`t_refreshTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '上一次刷新时间',
	`t_taskInfo` blob   COMMENT '玩家任务信息',
	PRIMARY KEY (`t_accountId`),
	KEY `t_accountId` (`t_accountId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;


alter table `t_userBaseAttr` add column `t_vipLevel` int UNSIGNED NOT NULL DEFAULT 0 COMMENT 'VIP等级';
alter table `t_userBaseAttr` add column	`t_todayRechargeMoney` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '今日累计充值金额';
alter table `t_userBaseAttr` add column	`t_todayExchangeMoney` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '今日累计提现金额';
alter table `t_userBaseAttr` add column	`t_lastExchangeMoneyTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '上一次提现时间';
