DROP TABLE IF EXISTS `t_gameGlobalExtInfo`;
CREATE TABLE `t_gameGlobalExtInfo` (
	`t_gameAtomTypeId` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '游戏id',
	`t_extInfo` blob   COMMENT '扩展信息',
	`t_updateTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
	PRIMARY KEY (`t_gameAtomTypeId`),
	KEY `t_gameAtomTypeId` (`t_gameAtomTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;