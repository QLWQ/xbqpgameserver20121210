
-- 记录最后一次通讯录上报时间
alter table `t_userBaseAttr` add column `t_reportAddressBookTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '上报通讯录时间';

DROP TABLE IF EXISTS `t_AddressBook`;
CREATE TABLE `t_AddressBook` (
	`t_accountID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '玩家id',
	`t_phone` varchar(126)  DEFAULT '' COMMENT '手机号码',
	`t_name` varchar(128)  DEFAULT '' COMMENT '姓名',
	`t_notes1` varchar(256) NOT NULL DEFAULT '' COMMENT '备注1',
	`t_notes2` varchar(256) NOT NULL DEFAULT '' COMMENT '备注2',
	`t_notes3` varchar(256) NOT NULL DEFAULT '' COMMENT '备注3',
	`t_updateTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '入库时间',
	`t_channelKey` varchar(50) NOT NULL DEFAULT '' COMMENT '渠道key',
	PRIMARY KEY (`t_accountID` , `t_phone`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
