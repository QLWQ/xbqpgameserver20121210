alter table `Configure_channelTask` add column `t_isContinueTask` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否连胜局数任务';


update `Configure_channelTask` set `t_isContinueTask` = 1 where t_taskId=10503;
update `Configure_channelTask` set `t_isContinueTask` = 1 where t_taskId=14103;
update `Configure_channelTask` set `t_isContinueTask` = 1 where t_taskId=15102;
update `Configure_channelTask` set `t_isContinueTask` = 1 where t_taskId=16101;
update `Configure_channelTask` set `t_isContinueTask` = 1 where t_taskId=20303;
update `Configure_channelTask` set `t_isContinueTask` = 1 where t_taskId=20403;
update `Configure_channelTask` set `t_isContinueTask` = 1 where t_taskId=20501;



update `Configure_channelTask` set `t_taskDesc` = '在百人牛牛连续胜利@targetNum@局(每局最低投注@coinLimit@金币)' where t_taskId=10503;
update `Configure_channelTask` set `t_taskDesc` =  '在五星宏辉押注连续胜利@targetNum@局(每局最低投注@coinLimit@金币)' where t_taskId=14103;
update `Configure_channelTask` set `t_taskDesc` =  '在龙虎斗押注连续胜利@targetNum@局(每局最低投注@coinLimit@金币)' where t_taskId=15102;
update `Configure_channelTask` set `t_taskDesc` =  '在百家乐押注连续胜利@targetNum@局(每局最低投注@coinLimit@金币)' where t_taskId=16101;
update `Configure_channelTask` set `t_taskDesc` =  '在飞禽走兽押注连续胜利@targetNum@局(每局最低投注@coinLimit@金币)' where t_taskId=20303;
update `Configure_channelTask` set `t_taskDesc` =  '在奔驰宝马押注连续胜利@targetNum@局(每局最低投注@coinLimit@金币)' where t_taskId=20403;
update `Configure_channelTask` set `t_taskDesc` =  '在21点连续胜利@targetNum@局(每局最低投注@coinLimit@金币)' where t_taskId=20501;

