alter table `UserTreasureChange` add column `KeyID` varchar(50) DEFAULT "" COMMENT '唯一ID' after ChannelNO;

--update UserTreasureChange set KeyID = BusinessGuid;


alter table `RecordGameRebate` drop column PromoteId, drop column Type, drop column RebateRate, drop column BusinessId;
alter table `RecordGameRebate` add column `KeyID` varchar(50) DEFAULT "" COMMENT '唯一ID' after ChannelNO;

alter table `RecordGameRebateDetail` add column `KeyID` varchar(50) DEFAULT "" COMMENT '唯一ID' after ChannelNO;


--添加业务类型
insert into configure_platformoperatorlog VALUES(2001022, 'cp_sub_coin', '从棋牌转到CP');
insert into configure_platformoperatorlog VALUES(2001023, 'cp_enter_fail', '从棋牌转到CP失败');
insert into configure_platformoperatorlog VALUES(2001024, 'cp_add_coin', '从CP转到棋牌');
insert into configure_platformoperatorlog VALUES(2001037, 'sys_rank_reward', '排行榜奖励');