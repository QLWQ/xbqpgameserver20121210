DROP TABLE IF EXISTS `Configure_channelTask`;
CREATE TABLE `Configure_channelTask` (
	`t_taskId` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '任务id',
	`t_isConver` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否对目标数量进行转换',
	`t_gameType` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '游戏类型',
	`t_gameName` varchar(64) NOT NULL DEFAULT '' COMMENT '游戏名称',
	`t_taskTitle` varchar(64) NOT NULL DEFAULT '' COMMENT '任务标题',
	`t_taskDesc` varchar(100) NOT NULL DEFAULT '' COMMENT '任务描述',
	PRIMARY KEY (`t_taskId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

insert into configure_platformoperatorlog VALUES(2001031, 'vip_upgrade_award', 'VIP专属晋级彩金');
insert into configure_platformoperatorlog VALUES(2001032, 'vip_week_award', 'VIP专属周礼金');
insert into configure_platformoperatorlog VALUES(2001033, 'vip_month_award', 'VIP专属月礼金');
insert into configure_platformoperatorlog VALUES(2001034, 'vip_add_signin_award', 'VIP专属签到奖励加成');
insert into configure_platformoperatorlog VALUES(2001035, 'recive_succour_award', '领取救济金');



insert into Configure_channelTask VALUES(10103, 1, 101, '经典斗地主', '积分任务',     '在经典斗地主累计赢取@targetNum@金币');
insert into Configure_channelTask VALUES(16101, 0, 161, '百家乐', 	  '押注任务',     '在百家乐押注连续胜利@targetNum@次');
insert into Configure_channelTask VALUES(10602, 0, 106, '金蝉捕鱼',   '捕鱼任务', 	  '在捕鱼捕获@targetNum@条金蟾');
insert into Configure_channelTask VALUES(20102, 1, 201, '抢庄牛牛',   '积分任务',     '在抢庄牛牛累计赢取@targetNum@金币');
insert into Configure_channelTask VALUES(10503, 0, 105, '百人牛牛',   '赌神任务',     '在百人牛牛连续胜利@targetNum@次');
insert into Configure_channelTask VALUES(20303, 0, 203, '飞禽走兽',   '赌神任务',     '在飞禽走兽押注连续胜利@targetNum@次');
insert into Configure_channelTask VALUES(20403, 0, 204, '奔驰宝马',   '赌神任务',     '在奔驰宝马押注连续胜利@targetNum@次');
insert into Configure_channelTask VALUES(19101, 1, 191, '红黑大战',   '积分任务',     '在红黑大战累计累计赢取@targetNum@金币');
insert into Configure_channelTask VALUES(15102, 0, 151, '龙虎斗', 	  '赌神任务',       '在龙虎斗押注连续胜利@targetNum@次');
insert into Configure_channelTask VALUES(17101, 1, 171, '炸金花', 	  '积分任务',     '在炸金花累计赢取@targetNum@金币');
insert into Configure_channelTask VALUES(14103, 0, 141, '花色嘉年华', '赌神任务',     '在花色嘉年华押注连续胜利@targetNum@次');
insert into Configure_channelTask VALUES(20001, 1, 200, '红包接龙',   '积分任务', 	  '在红包接龙累计赢取@targetNum@金币');
insert into Configure_channelTask VALUES(18101, 1, 181, '财神到', 	  '积分任务', 	  '在财神到累计赢取@targetNum@金币');
insert into Configure_channelTask VALUES(20201, 1, 202, '水果机', 	  '积分任务', 	  '在水果机累计赢取@targetNum@金币');
insert into Configure_channelTask VALUES(20501, 0, 205, '21点', 	  '赌神任务', 	  '在21点连续胜利@targetNum@次');
