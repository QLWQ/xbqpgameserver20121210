
--添加业务类型
insert into configure_platformoperatorlog VALUES(2001040, 'real_sub_coin', '从棋牌转到真人');
insert into configure_platformoperatorlog VALUES(2001041, 'real_enter_fail', '从棋牌转到真人失败');
insert into configure_platformoperatorlog VALUES(2001042, 'real_add_coin', '从真人转到棋牌');

insert into configure_platformoperatorlog VALUES(2001043, 'physical_sub_coin', '从棋牌转到体育');
insert into configure_platformoperatorlog VALUES(2001044, 'physical_enter_fail', '从棋牌转到体育失败');
insert into configure_platformoperatorlog VALUES(2001045, 'physical_add_coin', '从体育转到棋牌');

insert into configure_platformoperatorlog VALUES(2001046, 'electreic_sub_coin', '从棋牌转到电子');
insert into configure_platformoperatorlog VALUES(2001047, 'electreic_enter_fail', '从棋牌转到电子失败');
insert into configure_platformoperatorlog VALUES(2001048, 'electreic_add_coin', '从电子转到棋牌');

insert into configure_platformoperatorlog VALUES(2001049, 'fish_sub_coin', '从棋牌转到捕鱼');
insert into configure_platformoperatorlog VALUES(2001050, 'fish_enter_fail', '从棋牌转到捕鱼失败');
insert into configure_platformoperatorlog VALUES(2001051, 'fish_add_coin', '从捕鱼转到棋牌');