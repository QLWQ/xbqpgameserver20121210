alter table `RecordBoard151` add column `killInningIndex`   int(11) DEFAULT NULL COMMENT '强杀第几局';
alter table `RecordBoard151` add column `isCurInningKill`   int(11) DEFAULT NULL COMMENT '是否本局强杀, 1:是, 0：否';
alter table `RecordBoard151` add column `curActualTotalBet` int(11) DEFAULT NULL COMMENT '本局真人玩家龙虎实际总下注';
alter table `RecordBoard151` add column `killMaxTotalBet`   int(11) DEFAULT NULL COMMENT '强杀真人玩家龙虎最大总下注(强杀条件)';

