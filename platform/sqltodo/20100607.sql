alter table t_userBaseAttr add column `t_signIn` blob NOT NULL COMMENT '签到信息';
alter table t_userBaseAttr add column `t_lastSignInTime` int  NOT NULL DEFAULT 0 COMMENT '上次签到时间';

DROP TABLE IF EXISTS `t_signInChannel`;
CREATE TABLE `t_signInChannel` (
	`t_channel` varchar(64) NOT NULL DEFAULT '' COMMENT '渠道ID',
	`t_cfgInfo` blob   COMMENT '签到配置信息',
	`t_startTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '活动开始时间',
	`t_endTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '活动结束时间',
	`t_betLimit` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '每日下注限制',
	`t_openType` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '开关类型1开 0关',
	PRIMARY KEY (`t_channel`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_depositChannel`;
CREATE TABLE `t_depositChannel` (
	`t_channel` varchar(64) NOT NULL DEFAULT '' COMMENT '渠道ID',
	`t_cfgInfo` blob   COMMENT '充值返利配置信息',
	`t_startTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '活动开始时间',
	`t_endTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '活动结束时间',
	`t_openType` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '开关类型1开 0关',
	PRIMARY KEY (`t_channel`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_gameGlobalData`;
CREATE TABLE `t_gameGlobalData` (
	`t_gameAtomTypeId` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '游戏最小基本类型id',
	`t_gameInfo` blob   COMMENT '游戏信息',
	PRIMARY KEY (`t_gameAtomTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

