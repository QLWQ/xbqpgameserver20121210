DROP TABLE IF EXISTS `t_gameCtrlCfg`;
CREATE TABLE `t_gameCtrlCfg` (
	`t_gameAtomTypeId` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '游戏id',
	`t_cfgTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '配置时间',
	`t_cfgInfo` blob   COMMENT '游戏控制配置信息',
	PRIMARY KEY (`t_gameAtomTypeId`),
	KEY `t_gameAtomTypeId` (`t_gameAtomTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

