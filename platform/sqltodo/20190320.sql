alter table t_userBaseAttr add column `t_gameExData` blob   COMMENT '玩家游戏扩展数据';

alter table t_accountInfo drop column t_alipayRealName, drop column t_alipayID, drop column t_bankRealName, drop column t_bankID;
alter table t_accountInfo add column `t_alipayInfo` varchar(128)  DEFAULT '' COMMENT '支付宝账号信息';
alter table t_accountInfo add column `t_bankInfo` varchar(128)  DEFAULT '' COMMENT '支付宝账号信息';


DROP TABLE IF EXISTS `t_userbulletin`;
DROP TABLE IF EXISTS `t_bulletin`;
CREATE TABLE `t_bulletin` (
	`t_id` varchar(150) NOT NULL DEFAULT '' COMMENT '公告ID',
	`t_type` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '公告类型 1-公告栏 2-跑马灯',
	`t_title` varchar(150) NOT NULL DEFAULT '' COMMENT '公告标题',
	`t_content` blob NOT NULL  COMMENT '公告内容, 其中包括公告作用时间, 内容',
	`t_channelList` blob NOT NULL  COMMENT '影响渠道,json格式,记录所有渠道',
	`t_createTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
	PRIMARY KEY (`t_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_bulletin2Channel`;
CREATE TABLE `t_bulletin2Channel` (
	`t_bulletinId` varchar(150) NOT NULL DEFAULT '' COMMENT '公告ID',
	`t_channelId` varchar(16) NOT NULL DEFAULT '' COMMENT '渠道Id如果是所有渠道这里为0',
	PRIMARY KEY (`t_bulletinId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;