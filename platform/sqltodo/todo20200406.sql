DROP TABLE IF EXISTS `t_gameCtrlCfg_str`;
CREATE TABLE `t_gameCtrlCfg_str` (
	`t_key` varchar(64) NOT NULL DEFAULT '' COMMENT '唯一key',
	`t_cfgTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '配置时间',
	`t_cfgInfo` mediumblob   COMMENT '游戏控制配置信息',
	PRIMARY KEY (`t_key`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `t_singleGameCtrl`;
CREATE TABLE `t_singleGameCtrl` (
	`t_key` varchar(64) NOT NULL DEFAULT '' COMMENT '唯一key',
	`t_accountId` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '玩家ID',
	`t_gameAtomTypeId` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '游戏id',
	`t_newData` blob   COMMENT '单一控制信息',
	PRIMARY KEY (`t_key`),
	KEY `t_accountId` (`t_accountId`),KEY `t_gameAtomTypeId` (`t_gameAtomTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;