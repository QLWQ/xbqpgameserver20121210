DROP TABLE IF EXISTS `t_Bg`;
CREATE TABLE `t_Bg` (
	`t_accountID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
	`t_isInBg` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否在BG中',
	`t_isCoinComeBackFromBg` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否金币已从BG返回',
	`t_isGetBgDama` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否已从BG获取打码量',
	`t_enterBgTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '进入BG时间',
	`t_exitBgTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '退出BG时间',
	`t_isNotifyOpCoin2Bg` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否已通知Web来操作金币,携带到BG',
	`t_lastNotifyOpCoin2BgTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '上次通知Web来操作金币时间',
	`t_isCreate` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否已创建BG账号',
	PRIMARY KEY (`t_accountID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `t_promoterBindUrl`;
CREATE TABLE `t_promoterBindUrl` (
	`t_url` varchar(190) NOT NULL DEFAULT '' COMMENT '绑定url地址',
	`t_accountID` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
	`t_channel` varchar(32) NOT NULL DEFAULT '' COMMENT '渠道号',
	PRIMARY KEY (`t_url`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

alter table t_userBaseAttr add column `t_lastRechargeTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '最后充值时间，不计赠送' after `t_rechargeMoney`;