DROP TABLE IF EXISTS `t_channelDayShare`;
CREATE TABLE `t_channelDayShare` (
	`t_channel` varchar(64) NOT NULL DEFAULT '' COMMENT '渠道ID',
	`t_cfgTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '配置时间',
	`t_openType` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '开关类型',
	`t_dayShareInfo` blob   COMMENT '渠道救助金信息',
	PRIMARY KEY (`t_channel`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `t_channelXinboOnOff`;
CREATE TABLE `t_channelXinboOnOff` (
	`t_channel` varchar(64) NOT NULL DEFAULT '' COMMENT '渠道ID',
	`t_cfgTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '配置时间',
	`t_onOffInfo` blob   COMMENT '开关配置信息',
	PRIMARY KEY (`t_channel`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;


alter table `t_userBaseAttr` add column `t_dayShareCnt` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '每日分享次数';
alter table `t_userBaseAttr` add column	`t_lastDayShareTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '上次分享时间';



recordLog库：
insert into configure_platformoperatorlog VALUES(2001036, 'recive_day_share_award', '领取每日分享奖励');