#!/bin/sh

FILES="master dwh dbproxy name web state mail bulletin list hall scene game login shell brief AI collector relation"

echo "请输入需要发布的机器的IP地址:"
read ip

if [ ! -n "$ip" ]; then
	echo "IP地址不能为空"
	exit
fi

echo "请输入用户名(默认root):"
read username

echo "请输入发布目录(默认Home目录):"
read dir

IP=\"$ip\"

TARGET_DIR="publish"

if [ ! -d $TARGET_DIR ]; then
	mkdir -p $TARGET_DIR
else
	rm -rf $TARGET_DIR/*
fi

cp -R $PWD/../csv $TARGET_DIR
cp -R $PWD/../cfg $TARGET_DIR


for i in $FILES
do
	if [ -f $i ] ; then
		echo "copy $i"
		cp $i $TARGET_DIR
	else
		echo "$i not exist"
	fi
done

cd $TARGET_DIR/cfg
var=`cat common.xml|grep out_domain`
var=${var##*out_domain=}
var=${var% *}
eval sed -i 's/$var/$IP/g' common.xml

cd ../..
sleep 3
tar zcf $TARGET_DIR.tar.gz --exclude=$TARGET_DIR/csv/.svn $TARGET_DIR
rm -rf $TARGET_DIR

if [ ! -n "$UserName" ] ; then
	UserName="root"
fi

if [ ! -n "$dir" ] ; then
	dir="~"
fi

scp $TARGET_DIR.tar.gz $UserName@$ip:$dir

rm -rf $TARGET_DIR.tar.gz
