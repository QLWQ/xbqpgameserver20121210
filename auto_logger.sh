#! /bin/bash
num1=$(ps -ef | grep master|grep -v grep|wc -l)
if [ $num1 -eq 0 ]
then
	echo "master killed, no need start."
	exit
fi

num2=$(ps -ef | grep batchLog|grep logger1|grep -v grep|wc -l)
if [ $num2 -eq 0 ] 
then
	echo "batchLog not running, start ..."
	cd /app/new/platform/cfg && ./monitor_start_batchLog.sh logger1
else
	echo "logger is running"
fi


num3=$(ps -ef | grep batchLog|grep logger2|grep -v grep|wc -l)
if [ $num3 -eq 0 ] 
then
	echo "batchLog not running, start ..."
	cd /app/new/platform/cfg && ./monitor_start_batchLog.sh logger2
else
	echo "logger is running"
fi


num4=$(ps -ef | grep batchLog|grep logger3|grep -v grep|wc -l)
if [ $num4 -eq 0 ] 
then
	echo "batchLog not running, start ..."
	cd /app/new/platform/cfg && ./monitor_start_batchLog.sh logger3
else
	echo "logger is running"
fi


num5=$(ps -ef | grep batchLog|grep logger10|grep -v grep|wc -l)
if [ $num5 -eq 0 ] 
then
	echo "batchLog not running, start ..."
	cd /app/new/platform/cfg && ./monitor_start_batchLog.sh logger10
else
	echo "logger is running"
fi
