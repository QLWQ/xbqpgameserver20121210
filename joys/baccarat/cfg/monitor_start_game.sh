#!/bin/bash
#本脚本只是针对单个进程启动使用，适用场景是运维的监控系统自动调用
chmod 777 *

ulimit -c unlimited
ulimit -n 60000

curpath=$PWD
binPath=../bin

chmod +x $binPath/shell

$binPath/shell baccaratServerCfg.xml $1 $binPath/libGame $curpath & > /dev/null 2>&1